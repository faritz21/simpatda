<?php

namespace Pajak\Controller\Pendataan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Form\Pendataan\PendataanFrm;
use Pajak\Model\Pendataan\PendataanBase;
use Pajak\Helper\MenuHelper;

class Pendataan extends AbstractActionController {

    public function indexAction() {
        // echo 'gol'
        // exit();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $form = new \Pajak\Form\Pendaftaran\PendaftaranFrm($this->Tools()->getService('PendaftaranTable')->getcomboIdKecamatan(), null);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $s_idjenis = $this->getEvent()->getRouteMatch()->getParam('s_idjenis');
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($s_idjenis);
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();      
        $ar_pejabat_retribusi = $this->Tools()->getService('PejabatTable')->getTtdRetribusiBersangkutan($s_idjenis);

        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $recordspejabatretribusi = array();
        foreach ($ar_pejabat_retribusi as $ar_pejabat_retribusi_val) {
            $recordspejabatretribusi[] = $ar_pejabat_retribusi_val;
        }
        
        $view = new ViewModel(array(
            'form' => $form,
            's_idjenis' => $s_idjenis,
            'jenisobjek' => $jenisobjek,
            'ar_ttd0' => $recordspejabat,
            'ar_ttdretribusi' => $recordspejabatretribusi,
            "idjenis" => $this->params('s_idjenis'),
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            // 'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function menuPajakAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $s_idjenis = $this->getEvent()->getRouteMatch()->getParam('s_idjenis');
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($s_idjenis);
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $jmlpendataan_hotel = $this->Tools()->getService('PendataanTable')->getjmlpendataanHotel();
        $jmlpendataan_Restoran = $this->Tools()->getService('PendataanTable')->getjmlpendataanRestoran();
        $jmlpendataan_hiburan = $this->Tools()->getService('PendataanTable')->getjmlpendataanHiburan();
        $jmlpendataan_reklame = $this->Tools()->getService('PendataanTable')->getjmlpendataanReklame();
        $jmlpendataan_ppj = $this->Tools()->getService('PendataanTable')->getjmlpendataanPpj();
        $jmlpendataan_minerba = $this->Tools()->getService('PendataanTable')->getjmlpendataanMinerba();
        $jmlpendataan_parkir = $this->Tools()->getService('PendataanTable')->getjmlpendataanParkir();
        $jmlpendataan_air = $this->Tools()->getService('PendataanTable')->getjmlpendataanAir();
        $jmlpendataan_walet = $this->Tools()->getService('PendataanTable')->getjmlpendataanWalet();
        $view = new ViewModel(array(
            'datauser' => $session,
            's_idjenis' => $s_idjenis,
            'jenisobjek' => $jenisobjek,
            "idjenis" => $this->params('s_idjenis'),
            'dataobjek' => $recordspajak,
            'jmlpendataan_hotel' => $jmlpendataan_hotel,
            'jmlpendataan_restoran' => $jmlpendataan_Restoran,
            'jmlpendataan_hiburan' => $jmlpendataan_hiburan,
            'jmlpendataan_reklame' => $jmlpendataan_reklame,
            'jmlpendataan_ppj' => $jmlpendataan_ppj,
            'jmlpendataan_minerba' => $jmlpendataan_minerba,
            'jmlpendataan_parkir' => $jmlpendataan_parkir,
            'jmlpendataan_air' => $jmlpendataan_air,
            'jmlpendataan_walet' => $jmlpendataan_walet
        ));

        $data = array(
            'datauser' => $session
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridWpAction() {
         
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new \Pajak\Model\Pendaftaran\PendaftaranBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendaftaranTable')->getGridCountWp($base, $allParams['s_idjenis'], $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendaftaranTable')->getGridDataWp($base, $start, $allParams['s_idjenis'], $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        
        if($count == 0){
            $s .= "<tr><td colspan='15'>Tidak ada data</td></tr>";
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='NPWPD'><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_npwpd'] . "</center></td>";
            $s .= "<td data-title='Nama WP'>" . $row['t_nama'] . "</td>";
            $s .= "<td data-title='NIOP'><center style='font-size:12px;'>" . $row['t_nop'] . "</center></td>";
            $s .= "<td data-title='Nama OP'>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td data-title='Alamat OP'>" . $row['t_alamatobjek'] . "</td>";
            $s .= "<td data-title='Kelurahan OP'>" . $row['s_namakelobjek'] . "</td>";
            $s .= "<td data-title='Kecamatan OP'>" . $row['s_namakecobjek'] . "</td>";
            if ($row['t_status_wp'] == 't') {
                $cetakKartudata = "<button class='btn btn-primary btn-xs btn-flat' type='button' onclick='bukaCetakKartudata($row[t_idobjek])'><span class='glyph-icon icon-print'></span> Kartu Data</button>";
                if ($allParams['s_idjenis'] == 4) {
                    $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pagereklame?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                } else if ($allParams['s_idjenis'] == 6) {
                    $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pageminerba?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                } else if ($allParams['s_idjenis'] == 8) {
                    $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pageair?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                } else if (in_array($allParams['s_idjenis'], array(1,2,3,5,7,9))) {
                    $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pagedefault?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                } else {
                    $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pagedefaultret?t_idobjek=$row[t_idobjek]&s_idjenis=$row[s_idjenis]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i>Pendataan</a></center></td>";
                }
            } else {
                $s .= "<td data-title='#'><center><span class='btn btn-xs btn-flat btn-danger'><i class='glyph-icon icon-minus-circle'></i> OP TUTUP</span></center></td>";
            }
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "gridwp" => $s,
            "rowswp" => $base->rows,
            "countwp" => $count,
            "pagewp" => $page,
            "startwp" => $start,
            "total_halamanwp" => $total_pages,
            "paginatorewp" => $datapaging['paginatore'],
            "akhirdatawp" => $datapaging['akhirdata'],
            "awaldatawp" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridSudahAction() {
        
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $req = $this->getRequest();
        $post = $req->getPost();

        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendataanTable')->getGridCountSudah($base, $allParams['s_idjenis'], $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendataanTable')->getGridDataSudah($base, $start, $allParams['s_idjenis'], $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        if($count == 0){
            $s .= "<tr><td colspan='15'>Tidak ada data</td></tr>";
        }
        foreach ($data as $row) {
            $is_esptpd = ($row['is_esptpd'] != 0 ? '<span class="btn btn-xs btn-success">WP</span><br><b style="color:#db403c">e-SPTPD</b>' : '<span class="btn btn-xs btn-info">' . $ar_pemda->s_namasingkatinstansi . '</span><br><b style="color:#3850b8">SIMPATDA</b>');
            if($row['t_kodebayar']){
                $status_bayar = (!empty($row['t_tglpembayaran'])) ? '<b style="color:green;"><i class="glyph-icon icon-money"></i> LUNAS</b>' : '<b style="color:red;"><i class="glyph-icon icon-money"></i> BELUM BAYAR</b>';
            }else{
                $status_bayar = '<b style="color:blue;"><i class="glyph-icon icon-check-circle"></i> BELUM DITETAPKAN</b>';
            }

            $s .= "<tr>";
            $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='No. SPTPD'><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_nourut'] . "</center></td>";
            $s .= "<td data-title='Tanggal Pendataan'><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
            $s .= "<td data-title='NPWPD'><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_npwpd'] . "</center></td>";
            $s .= "<td data-title='Nama WP'>" . $row['t_nama'] . "</td>";
            $s .= "<td data-title='NIOP'><center>" . $row['t_nop'] . "</center></td>";
            $s .= "<td data-title='Nama Objek'>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td data-title='Masa Pajak' class='text-center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . ' s/d ' . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>";
//            if ($row['t_jenispajak'] == 4):
//                $s .= "<td data-title='Masa Pajak'>" . $row['t_lokasi'] . "</td>";
//            endif;
            $s .= "<td data-title='Nama Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>";
            $s .= "<td data-title='Jumlah Pajak' style='color:black; text-align: right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
            $s .= "<td data-title='KODE BAYAR' style='text-align: center;color:red;'><b>" . $row['t_kodebayar'] . "</b><BR>" . $status_bayar . "</td>";
            $s .= "<td data-title='Jumlah Pajak' style='text-align: center'>" . $is_esptpd . "</td>";
            $edit = "";
            $hapus = "";
            $operator = "";
			
			// if ($row['t_jenispajak'] == 4 || $row['t_jenispajak'] == 8) {
            //     $modelcetak = "<button onclick='bukaCetakSKPD($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak SKPD'><i class='glyph-icon icon-print'></i></button>";
            // } else {
                $modelcetak = "<button onclick='bukaCetakSPTPD($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i></button>";
            // }

            $cetak = $modelcetak;
			
            if (empty($row['t_tglpembayaran'])) {
                if ($row['t_jenispajak'] == 4) {
                    $edit = (empty($row['t_kodebayar'])) ? "<a href='form_pagereklameedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>" : "";
                }elseif ($row['t_jenispajak'] == 5) {
                    $edit = "<a href='form_pageppjedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                }elseif ($row['t_jenispajak'] == 6) {
                    $edit = "<a href='form_pageminerbaedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                } elseif ($row['t_jenispajak'] == 8) {
                    $edit = (empty($row['t_kodebayar'])) ? "<a href='form_pageairedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>" : "";
                } else {
                    $edit = "<a href='form_pagedefaultedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                }

                if ($session['s_akses'] == 2) {
                    $hapus = "<a href='#' onclick='hapus(" . $row['t_idtransaksi'] . ", ". $row['t_jenispajak'].");return false;' class='btn btn-danger btn-xs btn-flat' title='Hapus'><i class='glyph-icon icon-trash'></i></a>";
                    $operator = $row['s_nama'];
                }
				
				$cetak .= ($row['t_kodebayar']) ? " <button onclick='cetakkodebayar($row[t_idtransaksi])' class='btn btn-primary btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i></button>" : "";
				
            }else{
				$cetak .= ($row['t_kodebayar']) ? " <a href='cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' class='btn btn-primary btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i></a>" : "";
			}

            
            $s .= "<td data-title='#'><center> 
                    $cetak
                    $edit 
                    $hapus
                    <br>
                    $operator
                    </center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator1($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "gridsudah" => $s,
            "rowssudah" => $base->rows,
            "countsudah" => $count,
            "pagesudah" => $page,
            "startsudah" => $start,
            "total_halamansudah" => $total_pages,
            "paginatoresudah" => $datapaging['paginatore'],
            "akhirdatasudah" => $datapaging['akhirdata'],
            "awaldatasudah" => $datapaging['awaldata'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridMasahabisAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendataanTable')->getGridCountMasahabis($base, $allParams['s_idjenis'], $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendataanTable')->getGridDataMasahabis($base, $start, $allParams['s_idjenis'], $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='No. Urut'><center>" . $row['t_nourut'] . "</center></td>";
            $s .= "<td data-title='Tanggal Pendataan'><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
            $s .= "<td data-title='NPWPD'><center>" . $row['t_npwpd'] . "</center></td>";
            $s .= "<td data-title='Nama'>" . $row['t_nama'] . "</td>";
            $s .= "<td data-title='NIOP'><center>" . $row['t_nop'] . "</center></td>";
            $s .= "<td data-title='Nama Objek'>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td data-title='Masa Pajak'><center>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</center></td>";
            $s .= "<td data-title='Jumlah Pajak' style='color:black; text-align: right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
            $s .= "<td data-title='#'><center> <button onclick='bukaCetakHimbauan($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak Himbauan'><i class='glyph-icon icon-print'></i> Himbauan</button></center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator2($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid2" => $s,
            "rows2" => $base->rows,
            "count2" => $count,
            "page2" => $page,
            "start2" => $start,
            "total_halaman2" => $total_pages,
            "paginatore2" => $datapaging['paginatore'],
            "akhirdata2" => $datapaging['akhirdata'],
            "awaldata2" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridBelumLaporAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        $data = $this->Tools()->getService('PendataanTable')->getGridDataBelumLapor($base, $allParams['s_idjenis'], $allParams['page'], $allParams['rows']);
        $s = "";
        $counter = 1;
        $namapajak = '';
         
        foreach ($data as $row) {
            // lanjut jika katering atau boga
            if (trim($row['korek']) == '4.1.01.07.07.0001')
            {   
                continue;
            }

            $s .= "<tr>";
            $s .= "<td style='text-align:center'>" . $counter . "</td>";
            $s .= "<td style='text-align:center'><input type='checkbox' name='idobjekdbelumlapor' id='idobjekdbelumlapor' value='" . $row['t_idobjek'] . "'/></td>";
            $s .= "<td style='text-align:center'><b style='color:red; font-size:12px;'>" . $row['t_npwpdwp'] . "</b></td>";
            $s .= "<td>" . $row['t_namawp'] . "</td>";
            $s .= "<td style='text-align:center'>" . $row['t_nop'] . "</td>";
            $s .= "<td>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td>" . $row['t_alamatlengkapobjek'] . "</td>";
            $s .= "<td>" . $row['t_notelpobjek'] . "</td>";
            $s .= "<td>" . $row['t_nohpobjek'] . "</td>";
            $s .= "</tr>";
            $counter++;
            $namapajak = $row['s_namajenis'];
        }

        $s .= "<script type='text/javascript'>
                    $(document).ready(function () {
                        $('.tableBelumLapor tr').click(function (event) {
                            if (event.target.type !== 'checkbox') {
                                $(':checkbox', this).trigger('click');
                            }
                        }); 
                    });
                </script>";
        $htmlcount = '';
        $bulan = (int) $allParams['page'];
        // total belum lapor
        $counter--;
        $abulan = ['1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        // $htmlcount .= "<b style='color:red; animation: blinker 1s linear infinite;'>" . count($counter) . " OP $namapajak Belum Melaporkan SPTPD Pada Masa Pajak " . $abulan[$bulan] . " " . $allParams['rows'] . "</b>";
        $htmlcount .= "<b style='color:red; animation: blinker 1s linear infinite;'>" . $counter . " OP $namapajak Belum Melaporkan SPTPD Pada Masa Pajak " . $abulan[$bulan] . " " . $allParams['rows'] . "</b>";
        $data_render = array(
            "gridbelumlapor" => $s,
            "jumlah" => $htmlcount
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function FormPageinputteguranAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\TeguranlaporanFrm();
        if ($req->isGet()) {
            $idobjek = $req->getQuery()->get('idobjekbelumlapor');
            $bulanmasapajak = $req->getQuery()->get('bulanmasapajak');
            $tahunmasapajak = $req->getQuery()->get('tahunmasapajak');
            $s_idjenis = $req->getQuery()->get('s_idjenis');
            $abulan = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
            $masapajak = $abulan[$bulanmasapajak];

            $datamauditegur = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjekArray($idobjek);
            $maxnoteguran = $this->Tools()->getService('PendaftaranTable')->getNoteguran();
            $noteguran = (int) $maxnoteguran['t_noteguran'] + 1;

            $jenispajak = $this->Tools()->getService('PendaftaranTable')->JenisPajak($s_idjenis);
//            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new \Pajak\Model\Pendataan\TeguranBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                for ($i = 0; count($post['t_noteguran']) > $i; $i++) {
                    $this->Tools()->getService('TeguranTable')->simpansuratteguran($base, $post, $session, $i);
                }
                return $this->redirect()->toRoute("teguranlaporan", array("controllers" => "Teguranlaporan", "action" => "index"));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datamauditegur' => $datamauditegur,
            'noteguran' => $noteguran,
            'bulanpajak' => $bulanmasapajak,
            'masapajak' => $masapajak,
            'tahunpajak' => $tahunmasapajak,
            'jenispajak' => $jenispajak['s_namajenis']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }

        $belumditetapkan = $this->Tools()->getService('PenetapanTable')->getBelumDitetapkan();
        $data = array(
            'belumditetapkan' => $belumditetapkan,
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagedefaultAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);

            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            // auto insert jenis pendataan
            // if ($datapendataansebelumnya['s_idkorek'] == null) {
                $data->t_idkorek = $datapendaftaran['t_korekobjek'];
                $data->t_korek = $datapendaftaran['korek'];
                $data->t_namakorek = strtoupper($datapendaftaran['s_namakorek']);
                $data->t_tarifpajak = $datapendaftaran['s_persentarifkorek'];
            // }else{
				// $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
				// $data->t_korek = $datapendataansebelumnya['korek'];
				// $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
				// $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
			// }

            $data->t_tglpendataan = date('d-m-Y');
            if ($datapendataansebelumnya['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }

            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                
                    if ($post['t_jenispajak'] == 2 && $post['t_berdasarmasa'] == 'Tidak Berdasar Masa') {
                        $sudahditetapkan = null;
                    } else {
                        if (!empty($post['t_idtransaksi']))
                            $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                        else
                            $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                    }
                    
                    if (empty($sudahditetapkan)) {
                        $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                        $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                        return $this->redirect()->toRoute("pendataan", array(
                                    "controllers" => "Pendataan",
                                    "action" => "index",
                                    "page" => $req->getPost()['t_jenisobjekpajak']
                        ));
                    } else {
                        $id = $base->t_idobjek;
                        $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                        $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                        $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                        return $this->redirect()->toRoute("pendataan", array(
                                    "controllers" => "Pendataan",
                                    "action" => "index",
                                    "page" => $req->getPost()['t_jenisobjekpajak']
                        ));
                    }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
         
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa,
            'korek' => $datapendaftaran['korek'],
            'jenisobjek' => $datapendaftaran['t_jenisobjek']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }


    public function FormPagedefaultOldAction() {
             
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);

            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
             
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));


            // auto insert jenis pendataan
            if ($datapendataansebelumnya['s_idkorek'] == null) {
                $data->t_idkorek = $datapendaftaran['s_idkorek'];
                $data->t_korek = $datapendaftaran['korek'];
                $data->t_namakorek = $datapendaftaran['s_namakorek'];
                $data->t_tarifpajak = $datapendaftaran['s_persentarifkorek'];
            }

            $data->t_tglpendataan = date('d-m-Y');
            if ($datapendataansebelumnya['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }

            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
             
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {

            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            
          
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
//                var_dump($post);
//                die();
                if ($post['t_jenispajak'] == 2 && $post['t_berdasarmasa'] == 'Tidak Berdasar Masa') {
                    $sudahditetapkan = null;
                } else {
                    if (!empty($post['t_idtransaksi']))
                        $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                    else
//                         $sudahditetapkan = null;
                        $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }

                if (empty($sudahditetapkan)) {
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    // $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                    // $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    // $data->t_tglpendataan = date('d-m-Y');
                    // $data->t_idwp = $datapendaftaran['t_idwp'];
                    // $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
                    // $t_berdasarmasa = '';
                    // $form->bind($data);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
         
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa,
            'korek' => $datapendaftaran['korek'],
            'jenisobjek' => $datapendaftaran['t_jenisobjek']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagedefaulteditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            if ($datatransaksi['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }

            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];   
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
         
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'ar_ttd0' => $recordspejabat,
            'korek' => $data->t_korek,
            'jenisobjek' => $datapendaftaran['t_jenisobjek']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }



    public function FormPageppjoldAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanppjFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningPPJ();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            // $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            if ($datapendataansebelumnya['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $base->t_idkorek = 59;

                if (!empty($post['t_idtransaksi'])):
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                else:
//                     $sudahditetapkan = null;
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                endif;

                if (empty($sudahditetapkan)) {
//                    $file = $this->params()->fromFiles();
                    $data = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    $baseppj = new \Pajak\Model\Pendataan\DetailPpjBase();
                    $i = 0;
                    foreach ($post['t_idkorek'] as $row):
                        if ($post['t_iddetailppj'][$i] != NULL):
                            $baseppj->t_iddetailppj = $post['t_iddetailppj'][$i];
                        endif;
                        $baseppj->t_nilailistrik = $post['t_nilailistrik'][$i];
                        $baseppj->t_idkorek = $post['t_idkorek'][$i];
                        $baseppj->t_subtotalpajak = $post['t_subtotalpajak'][$i];
                        if ((int) $baseppj->t_subtotalpajak > 0):
                            $this->Tools()->getService('DetailPpjTable')->simpanpendataanppj($baseppj, $data);
                        endif;
                        $i++;
                    endforeach;
                    $baselampiran = new \Pajak\Model\Pendataan\LampiranPpjBase();
                    $i = 0;
                    $idToDelete = [];
                    if ((int) $post['t_total1'] > 0):

                        foreach ($post['t_goltarifpln1'] as $row):
                            if ($post['t_idlampiranppj1'][$i] != NULL):
                                $baselampiran->t_idlampiranppj = $post['t_idlampiranppj1'][$i];
                            endif;
                            $baselampiran->t_goltarifpln = $post['t_goltarifpln1'][$i];
                            $baselampiran->t_batasdaya = $post['t_batasdaya1'][$i];
                            $baselampiran->t_jmlpelanggan = $post['t_jmlpelanggan1'][$i];
                            $baselampiran->t_jmlkwhterjual = $post['t_jmlkwhterjual1'][$i];
                            $baselampiran->t_tarifperkwh = $post['t_tarifperkwh1'][$i];
                            $baselampiran->t_jmllistrikterjual = $post['t_jmllistrikterjual1'][$i];
                            $baselampiran->t_jmlbiayabeban = $post['t_jmlbiayabeban1'][$i];
                            $baselampiran->t_jmlnilaijuallistrik = $post['t_jmlnilaijuallistrik1'][$i];
                            $baselampiran->t_tarif = $post['t_tarif1'][$i];
                            $baselampiran->t_jumlah = $post['t_jumlah1'][$i];
                            $baselampiran->t_row = '1';

                            if ($post['t_idtransaksi'] != NULL):
                                $this->Tools()->getService('LampiranPpjTable')->simpanlampiranppj($baselampiran, $post);
                            else:
                                $this->Tools()->getService('LampiranPpjTable')->simpanlampiranppj($baselampiran, $data);
                            endif;

                            $i++;
                        endforeach;
                    endif;
                    $i = 0;
                    if ((int) $post['t_total2'] > 0):
                        foreach ($post['t_goltarifpln2'] as $row):
                            if ($post['t_idlampiranppj2'][$i] != NULL):
                                $baselampiran->t_idlampiranppj = $post['t_idlampiranppj2'][$i];
                            endif;
                            $baselampiran->t_goltarifpln = $post['t_goltarifpln2'][$i];
                            $baselampiran->t_batasdaya = $post['t_batasdaya2'][$i];
                            $baselampiran->t_jmlpelanggan = $post['t_jmlpelanggan2'][$i];
                            $baselampiran->t_jmlkwhterjual = $post['t_jmlkwhterjual2'][$i];
                            $baselampiran->t_tarifperkwh = $post['t_tarifperkwh2'][$i];
                            $baselampiran->t_jmllistrikterjual = $post['t_jmllistrikterjual2'][$i];
                            $baselampiran->t_jmlbiayabeban = $post['t_jmlbiayabeban2'][$i];
                            $baselampiran->t_jmlnilaijuallistrik = $post['t_jmlnilaijuallistrik2'][$i];
                            $baselampiran->t_tarif = $post['t_tarif2'][$i];
                            $baselampiran->t_jumlah = $post['t_jumlah2'][$i];
                            $baselampiran->t_row = '2';
                            if ($post['t_idtransaksi'] != NULL):
                                $this->Tools()->getService('LampiranPpjTable')->simpanlampiranppj($baselampiran, $post);
                            else:
                                $this->Tools()->getService('LampiranPpjTable')->simpanlampiranppj($baselampiran, $data);
                            endif;
                            $i++;
                        endforeach;
                    endif;
                    if ($post['t_idtransaksi'] != NULL):
                        foreach ($post['idToDelete'] as $col => $row):
                            $this->Tools()->getService('LampiranPpjTable')->deleteById($row);
                        endforeach;
                    endif;

                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    //     $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                    //     $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    //     $data->t_tglpendataan = date('d-m-Y');
                    //     $data->t_idwp = $datapendaftaran['t_idwp'];
                    //     $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
                    //     $t_berdasarmasa = '';
                    //     $form->bind($data);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa,
            'rekeningppj' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageppjeditAction() {
         
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
    
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_tglpenetapan = date('d-m-Y', strtotime($data->t_tglpenetapan));
            $data->t_jmlhpajak = $data->t_jmlhpajak;
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');
            
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getPendataanReklameByIdTransaksi($id);

            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];

            $datadetail = $this->Tools()->getService('DetailPpjTable')->getDetailByIdTransaksi($id);

            $datas = $this->Tools()->getService('ObjekTable')->getsemuaklasifikasi();
            $optionvalue = array();
            $index = 1;
            foreach ($datas as $key => $value)
            {
        
                $optionvalue[$index] = '0' . $index . ' || ' . $value['t_klasifikasi'];
                $index++;
            }
    
    
            $form = new \Pajak\Form\Pendataan\PendataanppjFrm($optionvalue);
            $form->bind($data);
            $golongan = $this->Tools()->getService('PendataanTable')->getGolonganPpj();
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'datadetail' => $datadetail,
            'message' => $message,
            'golongan' => $golongan
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagereklameAction() {

        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanreklameFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_tglpenetapan = date('d-m-Y', strtotime(date('d-m-Y')));
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
            $ar_jenisreklame = $this->Tools()->getService('ReklameTable')->getdata();
        }
        if ($this->getRequest()->isPost()) {    
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $data = $this->Tools()->getService('PendataanTable')->simpanpendataanofficial($base, $session, $post);
                $this->Tools()->getService('DetailreklameTable')->simpanpendataanreklame($post, $data);

                $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'ar_jenisreklame' => $ar_jenisreklame,
            'message' => $message
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    

    // public function FormPagereklameAction() {
    //     $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
    //     $req = $this->getRequest();
    //     $comboid_sudutpandang = $this->Tools()->getService('ReklameTable')->comboidSudutpandang();
    //     $comboid_zona = $this->Tools()->getService('ReklameTable')->comboidZona();
    //     $comboid_jenisreklame = $this->Tools()->getService('ReklameTable')->comboidJenisReklame();
    //     $comboid_klasifikasi_jalan = $this->Tools()->getService('ReklameTable')->comboidKlasifikasijalan();
    //     $form = new \Pajak\Form\Pendataan\PendataanreklameFrm($comboid_jenisreklame, $comboid_sudutpandang, $comboid_zona, $comboid_klasifikasi_jalan);
    //     if ($req->isGet()) {
    //         // echo 'it works';
    //         // exit();
    //         $id = (int) $req->getQuery()->get('t_idobjek');
    //         $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
    //         $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
    //         $ar_wilayah1 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(1);
    //         $ar_wilayah2 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(2);
    //         $ar_wilayah3 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(3);
    //         $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
    //         $data->t_idobjek = $datapendaftaran['t_idobjek'];
    //         $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
    //         $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
    //         $data->t_tglpendataan = date('d-m-Y');
    //         $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y')));
    //         $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . 'next year'));
    //         $data->t_tglpenetapan = date('d-m-Y', strtotime(date('d-m-Y')));
    //         $message = '';
    //         $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
    //         $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
    //         // var_dump($data);exit();
    //         $form->bind($data);
    //     }
    //     if ($this->getRequest()->isPost()) {
    //         echo 'gol';
    //         exit();
    //         $base = new PendataanBase();
    //         $basedetailreklame = new \Pajak\Model\Pendataan\DetailreklameBase();
    //         $form->setInputFilter($base->getInputFilter());
    //         $post = $req->getPost()->toArray();
    //         $form->setData($post);
    //         if ($form->isValid()) {
    //             $hasil = $form->getData();
    //             $base->exchangeArray($hasil);
    //             // var_dump($post);exit();
    //             $basedetailreklame->exchangeArray($hasil);
    //             $data = $this->Tools()->getService('PendataanTable')->simpanpendataanofficial($base, $session, $post);
    //             $this->Tools()->getService('DetailreklameTable')->simpanpendataanreklame($basedetailreklame, $data);
                
    //             $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
    //             // $this->flashMessenger()->addMessage('Pendataan Pajak Telah Tersimpan!');
    //             return $this->redirect()->toRoute("pendataan", array(
    //                 "controllers" => "Pendataan",
    //                 "action" => "index",
    //                 "page" => $req->getPost()['t_jenisobjekpajak']
    //             ));
    //         }
    //     }
    //     $view = new ViewModel(array(
    //         'form' => $form,
    //         'datapendaftaran' => $datapendaftaran,
    //         'data_wilayah1' => $ar_wilayah1,
    //         'data_wilayah2' => $ar_wilayah2,
    //         'data_wilayah3' => $ar_wilayah3,
    //         'message' => $message
    //     ));
    //     $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
    //     $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
    //     $recordspajak = array();
    //     foreach ($dataobjek as $dataobjek) {
    //         $recordspajak[] = $dataobjek;
    //     }
    //     $data = array(
    //         'data_pemda' => $ar_pemda,
    //         'datauser' => $session,
    //         'dataobjek' => $recordspajak
    //     );
    //     $this->layout()->setVariables($data);
    //     return $view;
    // }

    public function FormPagereklameeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_tglpenetapan = date('d-m-Y', strtotime($data->t_tglpenetapan));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getPendataanReklameByIdTransaksi($id);
            
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $datadetail = $this->Tools()->getService('DetailreklameTable')->getDetailReklameByIdTransaksi($id);
            $ar_jenisreklame = $this->Tools()->getService('ReklameTable')->getdata();
            $form = new \Pajak\Form\Pendataan\PendataanreklameFrm();
            $form->bind($data);

        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'ar_jenisreklame' => $ar_jenisreklame,
            'datadetail' => $datadetail,
            'message' => $message,
            't_jenisreklame' => $data->t_jenisreklame
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

//     public function FormPagereklameeditAction() {
//         $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
//         $req = $this->getRequest();
//         if ($req->isGet()) {
//             $message = '';
//             $id = (int) $req->getQuery()->get('t_idtransaksi');
//             $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//             $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
//             $ar_wilayah1 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(1);
//             $ar_wilayah2 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(2);
//             $ar_wilayah3 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(3);
//             $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
//             $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
//             $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
//             $data->t_tglpenetapan = date('d-m-Y', strtotime($data->t_tglpenetapan));
//             $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
//             $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

//             $datatransaksi = $this->Tools()->getService('PendataanTable')->getPendataanReklameByIdTransaksi($id);
//             $data->t_iddetailreklame = $datatransaksi['t_iddetailreklame'];
//             $data->t_jenisreklame = $datatransaksi['t_jenisreklame'];
//             $data->t_naskah = $datatransaksi['t_naskah'];
//             $data->t_lokasi = $datatransaksi['t_lokasi'];
//             $data->t_panjang = $datatransaksi['t_panjang'];
//             $data->t_lebar = $datatransaksi['t_lebar'];
//             $data->t_tinggi = $datatransaksi['t_tinggi'];
//             $data->t_jumlah = $datatransaksi['t_jumlah'];
//             $data->t_jangkawaktu = $datatransaksi['t_jangkawaktu'];
//             $data->t_tipewaktu = $datatransaksi['t_tipewaktu'];
//             $data->t_tarifreklame = $datatransaksi['t_tarifreklame'];
//             $data->t_wilayah = $datatransaksi['t_wilayah'];
//             $data->t_klasifikasi_jalan = $datatransaksi['t_klasifikasi_jalan'];
//             $data->t_jenisreklameusaha = $datatransaksi['t_jenisreklameusaha'];
//             $data->t_nsr = number_format($datatransaksi['t_nsr'], 0, ',', '.');
//             $data->t_sudutpandang = $datatransaksi['t_sudutpandang'];
//             $data->t_bentukreklame = $datatransaksi['t_bentukreklame'];

//             $data->t_idobjek = $datapendaftaran['t_idobjek'];
//             $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
//             $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
//             $data->t_idkorek = $datatransaksi['s_idkorek'];
//             $data->t_korek = $datatransaksi['korek'];
//             $data->t_namakorek = $datatransaksi['s_namakorek'];
//             $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
// //            var_dump($data);
// //            die();
//             $comboid_sudutpandang = $this->Tools()->getService('ReklameTable')->comboidSudutpandang();
//             $comboid_zona = $this->Tools()->getService('ReklameTable')->comboidZona();
//             $comboid_jenisreklame = $this->Tools()->getService('ReklameTable')->comboidJenisReklame();
//             $comboid_klasifikasi_jalan = $this->Tools()->getService('ReklameTable')->comboidKlasifikasijalan();
//             $form = new \Pajak\Form\Pendataan\PendataanreklameFrm($comboid_jenisreklame, $comboid_sudutpandang, $comboid_zona, $comboid_klasifikasi_jalan);
//             $form->bind($data);
//         }
//         $view = new ViewModel(array(
//             'form' => $form,
//             'datapendaftaran' => $datapendaftaran,
//             'data_wilayah1' => $ar_wilayah1,
//             'data_wilayah2' => $ar_wilayah2,
//             'data_wilayah3' => $ar_wilayah3,
//             'message' => $message,
//             't_jenisreklame' => $data->t_jenisreklame
//         ));
//         $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
//         $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
//         $recordspajak = array();
//         foreach ($dataobjek as $dataobjek) {
//             $recordspajak[] = $dataobjek;
//         }
//         $data = array(
//             'data_pemda' => $ar_pemda,
//             'datauser' => $session,
//             'dataobjek' => $recordspajak
//         );
//         $this->layout()->setVariables($data);
//         return $view;
//     }

    public function FormPageparkirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanparkirFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningParkir();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            // $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
//            $data->t_masaawal = date('01-m-Y');
//            $data->t_masaakhir = date('01-m-Y');
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());

                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }

                if (empty($sudahditetapkan)) {
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    $this->Tools()->getService('DetailparkirTable')->simpanpendataanparkir($post, $dataparent);

                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }

        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'ar_ttd0' => $recordspejabat,
            'message' => $message,
            'rekeningparkir' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageparkireditAction() {
        // var_dump('iki');exit();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanparkirFrm();
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailparkirTable')->getPendataanParkirByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailparkirTable')->getDetailParkirByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningParkir();
        $recordsrekening = array();
        foreach ($rekening as $rekening) {
            $recordsrekening[] = $rekening;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'rekeningparkir' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagePpjAction() {                 
         
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        // $form = new \Pajak\Form\Pendataan\PendataanminerbaFrm();
        $data = $this->Tools()->getService('ObjekTable')->getsemuaklasifikasi();
        $optionvalue = array();
        $index = 1;
        foreach ($data as $key => $value)
        {

            $optionvalue[$index] = '0' . $index . ' || ' . $value['t_klasifikasi'];   
            $index++;
        }


        $form = new \Pajak\Form\Pendataan\PendataanppjFrm($optionvalue);
        
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);

            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
             
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));


            // auto insert jenis pendataan
            if ($datapendataansebelumnya['s_idkorek'] == null) {
                $data->t_idkorek = $datapendaftaran['s_idkorek'];
                $data->t_korek = $datapendaftaran['korek'];
                $data->t_namakorek = $datapendaftaran['s_namakorek'];
                $data->t_tarifpajak = $datapendaftaran['s_persentarifkorek'];
            }

            $data->t_tglpendataan = date('d-m-Y');
            if ($datapendataansebelumnya['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));


             
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
         
             
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());

                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanppj($base, $session, $post);
                
                    $this->Tools()->getService('DetailPpjTable')->simpanpendataanppj($post, $dataparent);
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
            }
        }


        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $golongan = $this->Tools()->getService('PendataanTable')->getGolonganPpj();
        
         
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'korek' => $datapendaftaran['korek'],
            'jenisobjek' => $datapendaftaran['t_jenisobjek'],
            'golongan' => $golongan
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    //case added function
    public function hitungtotalpajakppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_pajak = $data_get['t_pajak1'] + $data_get['t_pajak2'] + $data_get['t_pajak3'] + $data_get['t_pajak4'] + $data_get['t_pajak5'] + $data_get['t_pajak6'] + $data_get['t_pajak7'] + $data_get['t_pajak8'] + $data_get['t_pajak9'] + $data_get['t_pajak10'];
        $t_pajakdibulatkan = number_format($t_pajak, 0, ",", ".");
        // 't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
        $data = array(
            't_jmlhpajak' => $t_pajak,
            't_jmlhpajakdibulatkan' => $t_pajakdibulatkan
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    // case added function
    public function hitungpajakppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        
        $jml_pemakaian = $data_get['t_pemakaian'];
        $tarif_dasar = $data_get['t_tarif_dasar'];
        $tarif_pajak = $data_get['t_tarif_pajak'];
        $dasar_pengenaan = $jml_pemakaian * $tarif_dasar;
        $dasar_pengenaandibulatkan = number_format($dasar_pengenaan, 0, ",", ".");

        $pajak_terhutang = ($dasar_pengenaan * $tarif_pajak) / 100;
        $pajak_terhutangdibulatkan = number_format($pajak_terhutang, 0, ",", ".");
        // $pajak_terhutang = number_format($pajak_terhutang, 0, ",", ".");  
        $data = array(
            'dasar_pengenaan' => $dasar_pengenaan,
            'pajak_terhutang' => $pajak_terhutang,
            'dasar_pengenaandibulatkan' => $dasar_pengenaandibulatkan,
            'pajak_terhutangdibulatkan' => $pajak_terhutangdibulatkan
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caridatagolonganAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
         
        // $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['t_idkorek']);
        $datappj = $this->Tools()->getService('PendataanTable')->getDataPpjById($data_get['t_idkorek']);
        // var_dump($dataRekening);exit();
         
        $data = array(
            // 't_hargapasaran' => number_format($dataRekening['s_tarifdasarkorek'], 0, ",", "."),
            // 't_tarifpersen' => $dataRekening['s_persentarifkorek']
            't_golongan' => $datappj[0]['t_golongan'],
            't_batas_daya' => $datappj[0]['t_batas_daya'],
            't_tarif_dasar' => $datappj[0]['t_tarif_dasar'],
            't_tarif_pajak' => $datappj[0]['t_tarif_pajak']

        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function FormPageminerbaAction() {  
                                          
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanminerbaFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            
            $indexrab = $this->Tools()->getService('DetailminerbaTable')->getIndexRab();
            $recordsindexrab = array();
            foreach ($indexrab as $indexrab) {
                $recordsindexrab[] = $indexrab;
            }
            
            $indexnonrab = $this->Tools()->getService('DetailminerbaTable')->getIndexNonRab();
            $recordsindexnonrab = array();
            foreach ($indexnonrab as $indexnonrab) {
                $recordsindexnonrab[] = $indexnonrab;
            }

            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));

            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
            $tarifrab = $this->Tools()->getService('DetailminerbaTable')->getTarifRab();
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
             
            
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                
                if ($post['t_jumlahnonrab'] >= 1000) {
                    $post['t_jumlahnonrab'] = str_replace(".","",$post['t_jumlahnonrab']);
                }
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataaneditminerba($base, $session, $post);
                    
                    if($base->t_golonganwapu == 1){
                        $this->Tools()->getService('DetailminerbaTable')->simpanpendataannonrabminerba($post, $dataparent);
                    }elseif($base->t_golonganwapu == 2){
                        $this->Tools()->getService('DetailminerbaTable')->simpanpendataanrabminerba($post, $dataparent);
                    }else{
                        $this->Tools()->getService('DetailminerbaTable')->simpanpendataanminerba($post, $dataparent);
                    }
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
            }
        }


        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $subrekening = $this->Tools()->getService('RekeningTable')->getSubrekeningMinerbaYangPunyaTarifDasar();
        
        $recordssubrekening =array();
        foreach($subrekening as $row)
        {
            $recordssubrekening[] = $row;
        }
        
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'rekeningmineral' => $recordsrekening,
            'indexrabmineral' => $recordsindexrab,
            'indexnonrabmineral' => $recordsindexnonrab,
            'subrek' => $recordssubrekening,
            'tarifrab' => $tarifrab
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function FormPageminerbaActionOld() {        
                  
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanminerbaFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            // //////////////////////////////////////////////
            // echo '<pre>';
            // print_r($req->getQuery());
            // echo '</pre>';
            // exit();
            // //////////////////////////////////////////////
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            // $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
//            $data->t_masaawal = date('01-m-Y');
//            $data->t_masaakhir = date('01-m-Y');
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            ////////////////////////////////////////////
            // echo '<pre>';
            // print_r($post);
            // echo '</pre>';
            // exit();
            ////////////////////////////////////////////
             
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
//                     $sudahditetapkan = '';
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }
//                 $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
//                     var_dump($sudahditetapkan); exit();
                if (empty($sudahditetapkan)) {
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    // var_dump($dataparent['t_idtransaksi']); exit();
                    $this->Tools()->getService('DetailminerbaTable')->simpanpendataanminerba($post, $dataparent);
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } 
                
                else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
//                     $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranId($id);
//                     $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
//                     $data->t_tglpendataan = date('d-m-Y');
//                     $data->t_idwp = $datapendaftaran['t_idwp'];
//                     $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
//                     $form->bind($data);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }


        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $subrekening = $this->Tools()->getService('RekeningTable')->getSubrekeningMinerba();
        
        $recordssubrekening =array();
        foreach($subrekening as $row)
        {
            $recordssubrekening[] = $row;
        }
        ////////////////////////////////////////////
        // echo '<pre>';
        // print_r($recordssubrekening);
        // echo '</pre>';
        // exit();
        ////////////////////////////////////////////
        
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'rekeningmineral' => $recordsrekening,
            'subrek' => $recordssubrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageminerbaeditAction() {
   
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanminerbaFrm();
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailminerbaTable')->getPendataanMinerbaByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $tarifrab = $this->Tools()->getService('DetailminerbaTable')->getTarifRab();
            $datadetailrab = $this->Tools()->getService('DetailminerbaTable')->getDetailRabMinerbaByIdTransaksi($id);
            $datadetailnonrab = $this->Tools()->getService('DetailminerbaTable')->getDetailNonRabMinerbaByIdTransaksi($id);
            $datadetail = $this->Tools()->getService('DetailminerbaTable')->getDetailMinerbaByIdTransaksi($id);
             
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        
        $indexrab = $this->Tools()->getService('DetailminerbaTable')->getIndexRab();
        $recordsindexrab = array();
        foreach ($indexrab as $indexrab) {
            $recordsindexrab[] = $indexrab;
        }
        
        $indexnonrab = $this->Tools()->getService('DetailminerbaTable')->getIndexNonRab();
        $recordsindexnonrab = array();
        foreach ($indexnonrab as $indexnonrab) {
            $recordsindexnonrab[] = $indexnonrab;
        }
        
        $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
        $recordsrekening = array();
        foreach ($rekening as $rekening) {
            $recordsrekening[] = $rekening;
        }

        $subrekening = $this->Tools()->getService('RekeningTable')->getSubrekeningMinerbaYangPunyaTarifDasar();
        $recordssubrekening =array();
        foreach($subrekening as $row)
        {
            $recordssubrekening[] = $row;
        }
         
        $view = new ViewModel(array(
            'form' => $form,
            'data' => $data,
            'tarifrab' => $tarifrab,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'datadetailrab' => $datadetailrab,
            'datadetailnonrab' => $datadetailnonrab,
            'indexrabmineral' => $recordsindexrab,
            'indexnonrabmineral' => $recordsindexnonrab,
            'rekeningmineral' => $recordsrekening,
            'subrek' => $recordssubrekening,
            'jenisobjekwp' => (!empty($data->t_jenisobjekwp)) ? $data->t_jenisobjekwp : 0,
            'golonganwapu' => (!empty($data->t_golonganwapu)) ? $data->t_golonganwapu : 0,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageairAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $combo = $this->Tools()->getService('AirTable')->getcomboPeruntukan();
        $form = new \Pajak\Form\Pendataan\PendataanairFrm($combo);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendaftaran['s_idkorek'];
            $data->t_korek = $datapendaftaran['korek'];
            $data->t_namakorek = strtoupper($datapendaftaran['s_namakorek']);
            $data->t_tarifpajak = $datapendaftaran['s_persentarifkorek'];
            $data->t_tglpendataan = date('d-m-Y');   
            $data->t_hargadasarair = $datapendaftaran['s_tarifdasarkorek'];
            $data->t_masaawal = (!empty($datapendataansebelumnya['t_masaawal'])) ? date('d-m-Y', strtotime($datapendataansebelumnya['t_masaawal'] . ' first day of next month ')) : date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month'));
            $data->t_masaakhir = (!empty($datapendataansebelumnya['t_masaakhir'])) ? date('d-m-Y', strtotime($datapendataansebelumnya['t_masaakhir'] . ' last day of next month ')) : date('d-m-Y', strtotime(date('t-m-Y') . 'last day of previous month'));
            $data->t_volume = number_format($datapendataansebelumnya['t_volume'], 0, ',', '.');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());

                // $sudahditetapkan = (!empty($post['t_idtraksaksi'])) ? $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]) : $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }

                if (empty($sudahditetapkan)) {
                    $this->Tools()->getService('PendataanTable')->simpanpendataanofficial($base, $session, $post);

                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageaireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $combo = $this->Tools()->getService('AirTable')->getcomboPeruntukan();
        $form = new \Pajak\Form\Pendataan\PendataanairFrm($combo);
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_peruntukan = $datatransaksi['t_peruntukanair'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $data->t_hargadasarair = number_format($datatransaksi['t_hargadasarair'], 0, ',', '.');
            $data->t_volume = $datatransaksi['t_volumeair'];
            $data->t_totalnpa = number_format($datatransaksi['t_totalnpa'], 0, ',', '.');
            $data->t_perhitungan = '(Rp. '.number_format($datatransaksi['t_totalnpa'], 0, ',', '.').' (Total NPA) x '.$datatransaksi['s_persentarifkorek'].'% (Tarif Pajak))';
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagewaletAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanwaletFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_tarifdasarkorek = number_format($datapendataansebelumnya['s_tarifdasarkorek'], 0, ',', '.');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $data->t_tglpendataan = date('d-m-Y');
            $data->t_masaawal = date('01-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_masaakhir = date('t-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $sudahditetapkan = (!empty($post['t_idtransaksi'])) ? $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]) : null;
                if (empty($sudahditetapkan)) {
                    $this->Tools()->getService('PendataanTable')->simpanpendataanwalet($base, $session, $post);

                    $this->flashMessenger()->addMessage('Pendataan Pajak Telah Tersimpan!');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranId($id);
                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    $data->t_tglpendataan = date('d-m-Y');
                    $data->t_idwp = $datapendaftaran['t_idwp'];
                    $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
                    $form->bind($data);
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagewaleteditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanwaletFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tarifdasarkorek = number_format($data->t_tarifdasarkorek, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');
            $message = '';

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // FORM RETRIBUSI
    /**
     * author : Roni Mustapa
     * email : ronimustapa@gmail.com
     * created : 09/04/2019
     */
    public function FormPagedefaultretAction() {

         
              $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
              $req = $this->getRequest();
              $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();


               
              $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
              if ($req->isGet()) {
                $id = (int) $req->getQuery()->get('t_idobjek');
                $idjenis = (int) $req->getQuery()->get('s_idjenis');
                $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                $data->t_idobjek = $datapendaftaran['t_idobjek'];
                $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
                $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
                $message = '';
                $data->t_idkorek = $datapendaftaran['s_idkorek']; 
                $data->t_korek = (string)$datapendaftaran['korek']; 
                $data->t_namakorek = (string)$datapendaftaran['s_namakorek']; 
                $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
                $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
                $form->bind($data);
            } // END GET

        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();

            // //////////////////////////////////////////////
            // echo '<pre>';
            // print_r($post);
            // echo '</pre>';
            // exit();
            // //////////////////////////////////////////////
             
            $explodeds_sub1korek = array();
            $counter = 0;
            foreach($post['s_sub1korek'] as $val)   
            {
                $explodeds_sub1korek[$counter] = explode("^",$val)[0] ;
                ++$counter;
            }
            $post['s_sub1korek'] = $explodeds_sub1korek;             
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailretribusiTable')->simpandetailretribusi($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }

        $subrekening = $this->Tools()->getService('PendataanTable')->getSubrekening($idjenis);
        $recordssubrekening =array();
        foreach($subrekening as $row)
        {
            $recordssubrekening[] = $row;
        }
        
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_tarif = $this->Tools()->getService('DetailparkirtepiTable')->getdataTarifParkir();
        $recordstarif = array();
        foreach ($ar_tarif as $ar_tarif) {
            $recordstarif[] = $ar_tarif;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'data_tarif' => $recordstarif,
            'datasubrekening' => $recordssubrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;

    }

    public function FormPageparkirtepiAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            // var_dump($post);exit();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                // var_dump($post);exit();
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailparkirtepiTable')->simpandetailparkirtepi($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_tarif = $this->Tools()->getService('DetailparkirtepiTable')->getdataTarifParkir();
        $recordstarif = array();
        foreach ($ar_tarif as $ar_tarif) {
            $recordstarif[] = $ar_tarif;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'data_tarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            // var_dump($post);exit();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailkirTable')->simpandetailretribusi($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_tarif = $this->Tools()->getService('DetailkirTable')->getdataTarif();
        $recordstarif = array();
        foreach ($ar_tarif as $ar_tarif) {
            $recordstarif[] = $ar_tarif;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'data_tarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepemadamAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpemadamFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $tarif = $this->Tools()->getService('DetailpemadamTable')->getdataTarifPemadam();
            $recordstarif = array();
            foreach ($tarif as $tarif) {
                $recordstarif[] = $tarif;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpenetapan = date('d-m-Y');
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailpemadamTable')->simpandetailpemadam($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'jenistarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageteraulangAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanteraFrm();
        if ($req->isGet()) {
            $id = $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $tarif = $this->Tools()->getService('DetailteraTable')->getdataTarifTera();
            $recordstarif = array();
            foreach ($tarif as $tarif) {
                $recordstarif[] = $tarif;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpenetapan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailteraTable')->simpanpendataantera($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'jenistarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagemenaraAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
//        $form = new \Pajak\Form\Pendataan\PendataanmenaraFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
//            $data->t_tarifdasar = $datapendataansebelumnya['s_tarifdasarkorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpenetapan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
//                    $this->Tools()->getService('DetailmenaraTable')->simpandetailmenara($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekekayaandaerahAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataankekayaandaerahFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $datakekayaan = $this->Tools()->getService('DetailkekayaanTable')->getdataJeniskekayaan();
            // var_dump('heyjude');exit();
            $recordsjenis = array();
            foreach ($datakekayaan as $datakekayaan) {
                $recordsjenis[] = $datakekayaan;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = strtoupper($datapendataansebelumnya['s_namakorek']);
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
//                var_dump($post); EXIT();
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailkekayaanTable')->simpandetail($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'jenislayanan' => $recordsjenis
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }


    public function FormPagekebersihanAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailkebersihanTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataankebersihanFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailkebersihanTable')->simpandetailkebersihan($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasarOldAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasarTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasarFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailpasarTable')->simpandetailpasar($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // tambahan ade
    public function FormPagePasarAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpasarFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getViewDataPasar();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            // $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
//            $data->t_masaawal = date('01-m-Y');
//            $data->t_masaakhir = date('01-m-Y');
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $post['t_idkorek'] = 103;
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                
                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }
                
                if (empty($sudahditetapkan)) {
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    if (!empty($base->t_idtransaksi)) {
                        $dataparent['t_idtransaksi'] = $base->t_idtransaksi;
                    }
                    // var_dump($dataparent);
                    // exit();
                    $this->Tools()->getService('DetailpasarTable')->simpandetailpasar($post, $dataparent);

                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Retribusi Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }

        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        // var_dump($recordsrekening);exit();
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'ar_ttd0' => $recordspejabat,
            'message' => $message,
            'data_tarif' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageparkirtepieditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            // $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            // $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_idtransaksi = $datapendaftaran['t_idtransaksi'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailparkirtepiTable')->getDetailParkirByIdTransaksi($id);
        }
        if ($this->getRequest()->isPost()) {
            // var_dump($this->getRequest());exit();
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailparkirtepiTable')->simpandetailparkirtepi($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                        ));
                    }
                }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_tarif = $this->Tools()->getService('DetailparkirtepiTable')->getdataTarifParkir();
        $recordstarif = array();
        foreach ($ar_tarif as $ar_tarif) {
            $recordstarif[] = $ar_tarif;
        }
        // var_dump($recordstarif);exit();
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'datadetail' => $datadetail,
            'ar_ttd0' => $recordspejabat,
            't_idtransaksi' => $data->t_idtransaksi,
            'data_tarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function caritarifpasarAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('RekeningTable')->getViewDataPasarId($data_get['t_idkorek']);
        // var_dump($data->current());exit();
        $data_render = array(
            "t_hargadasar" => number_format($data['s_tarifdasarkorek'], 0, ",", "."),
            "t_satuan" => $data['s_satuankorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
    // end

    public function FormPagepasargrosirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasargrosirTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasargrosirFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailpasargrosirTable')->simpandetailpasargrosir($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetempatparkirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboJeniskendaraan = $this->Tools()->getService('DetailtempatparkirTable')->getcomboIdJeniskendaraan();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataantempatparkirFrm($comboJeniskendaraan);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailtempatparkirTable')->simpandetailtempatparkir($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageterminalAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($post);exit();
                $this->Tools()->getService('DetailterminalTable')->simpandetailterminal($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_jenispel = $this->Tools()->getService('DetailterminalTable')->getdataJenis();
        $recordsjenispel = array();
        foreach ($ar_jenispel as $ar_jenispel) {
            $recordsjenispel[] = $ar_jenispel;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'data_jenispel' => $recordsjenispel
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageimbAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $imb_luas = $this->Tools()->getService('DetailimbTable')->getcomboIdImbluas();
        $imb_lantai = $this->Tools()->getService('DetailimbTable')->getcomboIdImblantai();
        $imb_gunabgn = $this->Tools()->getService('DetailimbTable')->getcomboIdImbgunabgn();
        $imb_lokasi = $this->Tools()->getService('DetailimbTable')->getcomboIdImblokasi();
        $imb_kondisi = $this->Tools()->getService('DetailimbTable')->getcomboIdImbkondisi();
        $form = new \Pajak\Form\Pendataan\PendataanimbFrm($imb_luas, $imb_lantai, $imb_gunabgn, $imb_lokasi, $imb_kondisi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailimbTable')->simpandetailimb($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        // $tarifdasar = $this->Tools()->getService('DetailimbTable')->getDataIdTarif(1);
        // var_dump($tarifdasar);exit();
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagehoAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $ho_kolasi = $this->Tools()->getService('DetailhoTable')->getcomboIdHolokasi();
        $recordskolasi = array();
            foreach ($ho_kolasi as $ho_kolasi) {
                $recordskolasi[] = $ho_kolasi;
            }
        $ho_gangguan = $this->Tools()->getService('DetailhoTable')->getcomboIdHogangguan();
        $ho_skala = $this->Tools()->getService('DetailhoTable')->getcomboIdHoskala();
        $form = new \Pajak\Form\Pendataan\PendataanhoFrm($ho_kolasi, $ho_gangguan, $ho_skala);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailhoTable')->simpandetailho($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datakolasi' => $recordskolasi,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetrayekAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantrayekFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            // var_dump($post);exit();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailtrayekTable')->simpandetailretribusi($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_tarif = $this->Tools()->getService('DetailtrayekTable')->getdataTarif();
        $recordstarif = array();
        foreach ($ar_tarif as $ar_tarif) {
            $recordstarif[] = $ar_tarif;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datatarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

//     public function FormPagetrayekAction() {
//         $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
//         $req = $this->getRequest();
//         $form = new \Pajak\Form\Pendataan\PendataantrayekFrm();
//         if ($req->isGet()) {
//             $id = (int) $req->getQuery()->get('t_idobjek');
//             $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
//             $tarif = $this->Tools()->getService('DetailtrayekTable')->getdataTarif();
//             $recordstarif = array();
//             foreach ($tarif as $tarif) {
//                 $recordstarif[] = $tarif;
//             }

//             $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
//             $data->t_idobjek = $datapendaftaran['t_idobjek'];
//             $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
//             $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
//             $message = '';
// //jika pernah melakukan pendataan sebelumnya
//             $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
//             $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
//             $data->t_korek = $datapendataansebelumnya['korek'];
//             $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
//             $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
//             $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
//             $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
//             $data->t_tglpendataan = date('d-m-Y');
//             $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
//             $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
//             $form->bind($data);
//         }
//         if ($this->getRequest()->isPost()) {
//             $base = new PendataanBase();
//             $form->setInputFilter($base->getInputFilter());
//             $post = $req->getPost()->toArray();
//             // var_dump($post);exit();
//             $form->setData($post);
//             // var_dump($form);exit();
//             if ($form->isValid()) {
//                 $base->exchangeArray($form->getData());
                
//                 $dataparent = 
//                 $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
//                 // var_dump($dataparent);exit();
//                 $this->Tools()->getService('DetailtrayekTable')->simpandetailretribusi($post, $dataparent);
//                 // var_dump($coba);exit();
//                 return $this->redirect()->toRoute("pendataan", array(
//                             "controllers" => "Pendataan",
//                             "action" => "index",
//                             "page" => $req->getPost()['t_jenisobjekpajak']
//                 ));
//             }
//         }
//         $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
//         $recordspejabat = array();
//         foreach ($ar_pejabat as $ar_pejabat) {
//             $recordspejabat[] = $ar_pejabat;
//         }
//         $view = new ViewModel(array(
//             'form' => $form,
//             'datapendaftaran' => $datapendaftaran,
//             'message' => $message,
//             'ar_ttd0' => $recordspejabat,
//             'datatarif' => $recordstarif,
//         ));
//         $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
//         $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
//         $recordspajak = array();
//         foreach ($dataobjek as $dataobjek) {
//             $recordspajak[] = $dataobjek;
//         }
//         $data = array(
//             'data_pemda' => $ar_pemda,
//             'datauser' => $session,
//             'dataobjek' => $recordspajak
//         );
//         $this->layout()->setVariables($data);
//         return $view;
//     }

    public function FormPageperikananAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $data_tarifKapal = $this->Tools()->getService('DetailperikananTable')->getcomboIdKapal();
        $data_tarifBudidaya = $this->Tools()->getService('DetailperikananTable')->getcomboIdBudidaya();
        $data_tarifBudidayaMutiara = $this->Tools()->getService('DetailperikananTable')->getcomboIdBudidayaMutiara();
        $form = new \Pajak\Form\Pendataan\PendataanperikananFrm($data_tarifKapal, $data_tarifBudidaya, $data_tarifBudidayaMutiara);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailperikananTable')->simpandetailperikanan($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    /*
      EDIT RETRIBUSI
     */

    public function FormPageDefaultreteditAction() {
        //////////////////////////////////////////////
        // echo '<pre>';
        // print_r('gol');
        // echo '</pre>';
        // exit();
        // //////////////////////////////////////////////
         
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $idjenis = (int) $req->getQuery()->get('s_idjenis');

            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpenetapan = date('d-m-Y', strtotime($datatransaksi['t_tglpenetapan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_namakegiatan = $datatransaksi['t_namakegiatan'];
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getDetailKesehatanByIdTransaksi($id);
//            var_dump($form->bind($data)); exit();
        }
        // ////////////////////////////////////////////////////////////////////////////////
        // echo '<pre>';
        // echo ('$datatransaksi');
        // print_r($datatransaksi);
        // echo '</br>========================================================================';
        // echo '</pre>';
         
        // exit();
        // ////////////////////////////////////////////////////////////////////////////////
         

        $subrekening = $this->Tools()->getService('PendataanTable')->getSubrekening($idjenis);

        $recordssubrekening =array();
        foreach($subrekening as $row)
        {
            $recordssubrekening[] = $row;
        }

        //////////////////////////////////////////////
        // echo '<pre>';
        // print_r($recordssubrekening);
        // echo '</pre>';
        // exit();
        //////////////////////////////////////////////
         
        
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'datatransaksi' => $datatransaksi,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'datasubrekening' => $recordssubrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekesehataneditAction() {

        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataandefaultretFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $idjenis = (int) $req->getQuery()->get('s_idjenis');

            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpenetapan = date('d-m-Y', strtotime($datatransaksi['t_tglpenetapan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_namakegiatan = $datatransaksi['t_namakegiatan'];
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getDetailKesehatanByIdTransaksi($id);
//            var_dump($form->bind($data)); exit();
        }
        //////////////////////////////////////////////////////////////////////////////////
        // echo '<pre>';
        // echo ('$datatransaksi');
        // print_r($datatransaksi);
        // echo '</br>========================================================================';
        // echo '</pre>';
         
        // exit();
        //////////////////////////////////////////////////////////////////////////////////
         

        $subrekening = $this->Tools()->getService('PendataanTable')->getSubrekening($idjenis);

        $recordssubrekening =array();
        foreach($subrekening as $row)
        {
            $recordssubrekening[] = $row;
        }
        
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'datatransaksi' => $datatransaksi,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'datasubrekening' => $recordssubrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekebersihaneditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailkebersihanTable')->getcomboIdKlasifikasi();
        $form = new \Pajak\Form\Pendataan\PendataankebersihanFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailkebersihanTable')->getPendataanKebersihanByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idkebersihan'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_klasifikasi = $datatransaksi['t_idklasifikasi'];
            $data->t_kategori = $datatransaksi['t_idtarif'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasareditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasarTable')->getcomboIdKlasifikasi();
        $form = new \Pajak\Form\Pendataan\PendataanpasarFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            // $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            // // $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            // $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            // $data->t_idobjek = $datapendaftaran['t_idobjek'];
            // $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            // $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            // $message = '';
            
            $datatransaksi = $this->Tools()->getService('DetailpasarTable')->getPendataanPasarByIdTransaksi($id);
            // $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            // $data->t_idkorek = $datatransaksi['t_idkorek'];
            // $data->t_nourut = $datatransaksi['t_nourut'];
            // $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            // $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            // $data->t_idkorek = $datatransaksi['s_idkorek'];
            // $data->t_korek = $datatransaksi['korek'];
            // $data->t_namakorek = $datatransaksi['s_namakorek'];
            // $data->t_tglpenetapan = $datatransaksi['t_tglpenetapan'];
            // $form->bind($data);
            
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            
            //            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            // $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            // $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            // var_dump($datadetail->current());
            // echo '<br><br>';
            // var_dump($datapendaftaran);
            // exit();
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailpasarTable')->getDetailPasarByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $rekening = $this->Tools()->getService('RekeningTable')->getViewDataPasar();
        $recordsrekening = array();
        foreach ($rekening as $rekening) {
            $recordsrekening[] = $rekening;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'data_tarif' => $recordsrekening,
            'datadetail' => $datadetail,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        // var_dump($recordspajak);exit();
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            // 'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepemadameditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpemadamFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailpemadamTable')->getPendataanPemadamByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idretpemadam = $datatransaksi['t_idretpemadam'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailpemadamTable')->getDetailPemadamByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $tarif = $this->Tools()->getService('DetailpemadamTable')->getdataTarifPemadam();
        $recordstarif = array();
        foreach ($tarif as $tarif) {
            $recordstarif[] = $tarif;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'jenistarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageteraulangeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanteraFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailteraTable')->getPendataanTeraByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrettera = $datatransaksi['t_idrettera'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailteraTable')->getDetailTeraByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $tarif = $this->Tools()->getService('DetailteraTable')->getdataTariftera();
        $recordstarif = array();
        foreach ($tarif as $tarif) {
            $recordstarif[] = $tarif;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'jenistarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagemenaraeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekekayaandaeraheditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataankekayaandaerahFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $datakekayaan = $this->Tools()->getService('DetailkekayaanTable')->getdataJeniskekayaan();
            $recordsjenis = array();
            foreach ($datakekayaan as $datakekayaan) {
                $recordsjenis[] = $datakekayaan;
            }
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpenetapan = date('d-m-Y', strtotime($datatransaksi['t_tglpenetapan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_namakegiatan = $datatransaksi['t_namakegiatan'];
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailkekayaanTable')->getDetailKekayaanByIdTransaksi($id);
//            var_dump($form->bind($data)); exit();
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'jenislayanan' => $recordsjenis
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepengujiankendaraanbermotoreditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataankekayaandaerahFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            // $datakekayaan = $this->Tools()->getService('DetailkekayaanTable')->getdataJeniskekayaan();
            $ar_tarif = $this->Tools()->getService('DetailkirTable')->getdataTarif();
            $recordstarif = array();
            foreach ($ar_tarif as $ar_tarif) {
                $recordstarif[] = $ar_tarif;
            }
            
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpenetapan = date('d-m-Y', strtotime($datatransaksi['t_tglpenetapan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_namakegiatan = $datatransaksi['t_namakegiatan'];
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailkirTable')->getDetailPengujianKendaraanBermotorByIdTransaksi($id);
//            var_dump($form->bind($data)); exit();
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'data_tarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetrayekeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantrayekFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            // $datakekayaan = $this->Tools()->getService('DetailkekayaanTable')->getdataJeniskekayaan();
            $ar_tarif = $this->Tools()->getService('DetailtrayekTable')->getdataTarif();
            $recordstarif = array();
            foreach ($ar_tarif as $ar_tarif) {
                $recordstarif[] = $ar_tarif;
            }
            
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpenetapan = date('d-m-Y', strtotime($datatransaksi['t_tglpenetapan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_namakegiatan = $datatransaksi['t_namakegiatan'];
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailtrayekTable')->getPendataanTrayekByIdTransaksi($id);
//            var_dump($form->bind($data)); exit();
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'data_tarif' => $recordstarif
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasargrosireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasargrosirTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasargrosirFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailpasargrosirTable')->getPendataanPasargrosirByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idpasar'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_klasifikasi = $datatransaksi['t_idklasifikasi'];
            $data->t_jenisbangunan = $datatransaksi['t_jenisbangunan'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetempatparkireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboJeniskendaraan = $this->Tools()->getService('DetailtempatparkirTable')->getcomboIdJeniskendaraan();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataantempatparkirFrm($comboJeniskendaraan);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailtempatparkirTable')->getPendataanTempatparkirByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idpasar'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_jeniskendaraan = $datatransaksi['t_jeniskendaraan'];
            $data->t_jmlhkendaraan = $datatransaksi['t_jumlah'];
            $data->t_keterangan = $datatransaksi['t_keterangan'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagerumahdinasAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanrumahdinasFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailrumahdinasTable')->simpandetailrumahdinas($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagerumahdinaseditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanrumahdinasFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailrumahdinasTable')->getPendataanRumahDinasByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idrumahdinas'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepanggungreklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpanggungreklameFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailpanggungreklameTable')->simpandetailpanggungreklame($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepanggungreklameeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpanggungreklameFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailpanggungreklameTable')->getPendataanPanggungReklameByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrpangrek = $datatransaksi['t_idrpangrek'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetanahreklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahreklameFrm($this->Tools()->getService('DetailtanahreklameTable')->getcomboIdTanahReklame());
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailtanahreklameTable')->simpandetailtanahreklame($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetanahreklameeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahreklameFrm($this->Tools()->getService('DetailtanahreklameTable')->getcomboIdTanahReklame());
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailtanahreklameTable')->getPendataanTanahReklameByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrpangrek = $datatransaksi['t_idrpangrek'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_jmlhblnhari = $datatransaksi['t_jmlhblnhari'];
            $data->t_lokasitanah = $datatransaksi['t_lokasitanah'];
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function CariTarifpipaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // $t_volume = $data_get['t_tarifdasarkorek'] * $data_get['t_jmlhsumur'];
        $data_tarifpipa = $this->Tools()->getService('AirTable')->getdataTarifpipaId($data_get['t_kodekelompok']);

        if ($data_get['t_idkorek'] == 103) {
            $t_volume = $data_tarifpipa['s_pipa1'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 104) {
            $t_volume = $data_tarifpipa['s_pipa2'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 105) {
            $t_volume = $data_tarifpipa['s_pipa3'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 106) {
            $t_volume = $data_tarifpipa['s_pipa4'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 107) {
            $t_volume = $data_tarifpipa['s_pipa5'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 108) {
            $t_volume = $data_tarifpipa['s_pipa6'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 109) {
            $t_volume = $data_tarifpipa['s_pipa7'] * $data_get['t_jmlhsumur'];
        }

        $data = array(
            't_volume' => $t_volume,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function CariKodeJenisAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_kodejenis = $this->Tools()->getService('AirTable')->getdataKodeJenisId($data_get['t_kodekelompok']);
        $data_jenis .= '<option value="">Pilih Kode Jenis</option>';
        foreach ($data_kodejenis as $row):
            $data_jenis .= '<option value="' . $row['s_id'] . '">' . $row['s_nourut'] . ' [ ' . $row['s_deskripsi'] . ' ]</option>';
        endforeach;
        $dataToRender = [
            't_kodejenis' => $data_jenis,
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($dataToRender));
    }

    public function caritariftanahreklameAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailtanahreklameTable')->getDataId($data_get['t_lokasitanah']);
        $data_render = array(
            "t_tarifdasar" => number_format($data['s_tarif'], 0, ",", "."),
            "s_satuan" => $data['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function FormPagetanahlainAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahlainFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailtanahlainTable')->simpandetailtanahlain($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetanahlaineditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahlainFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailtanahlainTable')->getPendataanTanahLainByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrpangrek = $datatransaksi['t_idrpangrek'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagegedungAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataangedungFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $tarifgedung = $this->Tools()->getService('DetailgedungTable')->gettarifgedung();
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailgedungTable')->simpandetailgedung($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $recordsgedung = array();
        foreach ($tarifgedung as $tarifgedung) {
            $recordsgedung[] = $tarifgedung;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'tarifgedung' => $recordsgedung
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagegedungeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataangedungFrm();
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailgedungTable')->getPendataanGedungByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailgedungTable')->getDetailGedungByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $tarifgedung = $this->Tools()->getService('DetailgedungTable')->gettarifgedung();
        $recordsgedung = array();
        foreach ($tarifgedung as $tarifgedung) {
            $recordsgedung[] = $tarifgedung;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'tarifgedung' => $recordsgedung
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function caritarifgedungAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailgedungTable')->gettarifgedungId($data_get['t_idtarifgedung']);
        $data_render = array(
            "t_tarif" => number_format($data['s_tarif'], 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretgedungAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_tarif = str_ireplace(".", "", $data_get['t_tarif']);
        $t_total = $t_tarif * $data_get['t_jumlah'];
        $data_render = array(
            "t_total" => number_format($t_total, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungtotalretgedungAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_total1']) + str_ireplace(".", "", $data_get['t_total2']) + str_ireplace(".", "", $data_get['t_total3']) +
                str_ireplace(".", "", $data_get['t_total4']) + str_ireplace(".", "", $data_get['t_total5']) + str_ireplace(".", "", $data_get['t_total6']) +
                str_ireplace(".", "", $data_get['t_total7']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretimbluasAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tarifdasar = $this->Tools()->getService('DetailimbTable')->getDataIdTarif(1);
        $t_jumlah = str_ireplace(".", "", $data_get['t_jmlhbangunan']) * $data_get['t_panjang'] * $data_get['t_lebar'];

        $data = array(
            't_luas' => number_format($t_jumlah, 2, ".", "."),
            't_tarifdasar' => number_format($tarifdasar['s_tarif'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalretimbAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // var_dump($data_get);exit();
        $koe_luas = $this->Tools()->getService('DetailimbTable')->getDataIdKoefisienluas($data_get['t_imbluas']);

        $koe_lantai = $this->Tools()->getService('DetailimbTable')->getDataIdKoefisienlantai($data_get['t_imblantai']);
        // var_dump($koe_lantai);exit();
        $koe_gunabgn = $this->Tools()->getService('DetailimbTable')->getDataIdKoefisiengunabgn($data_get['t_imbgunabgn']);
        $koe_lokasi = $this->Tools()->getService('DetailimbTable')->getDataIdKoefisienlokasi($data_get['t_imblokasi']);
        $koe_kondisi = $this->Tools()->getService('DetailimbTable')->getDataIdKoefisienkondisi($data_get['t_imbkondisi']);
        $jumlah_koef = $koe_luas['s_koefisien'] * $koe_lantai['s_koefisien'] * $koe_gunabgn['s_koefisien'] * $koe_lokasi['s_koefisien'] * $koe_kondisi['s_koefisien'];
        // var_dump($jumlah_koef);exit();

        $data = array(
            't_jmlhpajak' => number_format($jumlah_koef * $data_get['t_luas'] * str_ireplace(".", "", $data_get['t_tarifdasar']), 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungretholuasAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_panjang']) * str_ireplace(".", "", $data_get['t_lebar']);
        $data = array(
            't_luas' => number_format($t_jumlah, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalrethoAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $indeks_lokasi = $this->Tools()->getService('DetailhoTable')->getDataIdIndekslokasi($data_get['t_kwslokasi']);
        $indeks_gangguan = $this->Tools()->getService('DetailhoTable')->getDataIdIndeksgangguan($data_get['t_gangguan']);
        $indeks_luas = $this->Tools()->getService('DetailhoTable')->getDataIdIndeksluas($data_get['t_luas']);
        $tarifdasar = $this->Tools()->getService('DetailhoTable')->getDataIdTarif($data_get['t_skala']);
        $jumlah_indeks = $indeks_lokasi['s_indeks'] * $indeks_gangguan['s_indeks'] * $indeks_luas['s_indeks'];
        $data = array(
            't_indeks_luas' => $indeks_luas['s_indeks'],
            't_tarifdasar' => number_format($tarifdasar['s_tarif'], 0, ",", "."),
            't_jmlhpajak' => number_format($jumlah_indeks * $tarifdasar['s_tarif'], 0, ",", "."),
//            't_jmlhpajak' => $indeks_luas['s_indeks'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungretkapalAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // $tarifdasar_kapal = $this->Tools()->getService('DetailperikananTable')->getDataIdKapal($data_get['t_jeniskapal']);
        // $t_jumlah = $tarifdasar_kapal['s_tarif'] * str_ireplace(".", "", $data_get['t_jmlhgt']);
        $data = array(
            // 't_tarifdasar' => number_format($tarifdasar_kapal['s_tarif'], 0, ",", "."),
            't_satuan' => $tarifdasar_kapal['s_satuan'],
            't_jmlhpajak' => number_format($data_get['t_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function carikekayaandaerahAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailkekayaanTable')->getdataKategorisatu($data_get['t_klasifikasi']);
        $data_jenis .= '<option value="">Silahkan Pilih</option>';
        foreach ($data as $row):
            $data_jenis .= '<option value="' . $row['s_idkategorisatu'] . '">' . strtoupper($row['s_keterangan']) . '</option>';
        endforeach;
        $dataToRender = [
            't_kategori' => $data_jenis,
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($dataToRender));
    }

    public function carikategorikekayaanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($data_get['t_kategorisatu'] == 3) {
            $data1 = $this->Tools()->getService('DetailkekayaanTable')->getdataTarifkategori(3);
        } else if ($data_get['t_kategorisatu'] == 1 || $data_get['t_kategorisatu'] == 2 || $data_get['t_kategorisatu'] == 4 || $data_get['t_kategorisatu'] == 5 || $data_get['t_kategorisatu'] == 6 || $data_get['t_kategorisatu'] == 7 || $data_get['t_kategorisatu'] == 8 || $data_get['t_kategorisatu'] == 9) {
            $data = $this->Tools()->getService('DetailkekayaanTable')->getdataKategoridua($data_get['t_kategorisatu']);
            $data_jenis .= '<option value="">Silahkan Pilih</option>';
            foreach ($data as $row):
                $data_jenis .= '<option value="' . $row['s_idtarif'] . '">' . strtoupper($row['s_keterangan']) . '</option>';
            endforeach;
        } else {
            $data2 = $this->Tools()->getService('DetailkekayaanTable')->getdataTarifkategori($data_get['t_kategorisatu']);
        }
        $dataToRender = [
            't_kategori' => $data_jenis,
            't_tarifret' => $data1['s_tarif'],
            't_tarif' => number_format($data2['s_tarif'], 0, ",", "."),
            't_satuan' => $data1['s_satuan'],
            't_satuan2' => $data2['s_satuan'],
            't_idkategori' => $data_get['t_kategorisatu']
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($dataToRender));
    }

    public function caritarifkekayaanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $ar_tarif = $this->Tools()->getService('DetailkekayaanTable')->getdataTarif($data_get['t_idtarif']);
//        var_dump($ar_tarif);exit();
        $data = array(
            't_satuan' => $ar_tarif['s_satuan'],
            't_nilaitanah' => number_format($ar_tarif['s_nilaitanah'], 0, ",", "."),
            't_nilaibangunan' => number_format($ar_tarif['s_nilaibangunan'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungretribusikekayaanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
//        $datatarif = $this->Tools()->getService('KekayaandaerahTable')->getDataKlasifikasi($data_get['t_klasifikasi']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        if ($data_get['t_kategorisatu'] == 3) {
            $t_luastanah = $data_get['t_jmlhbln'] * str_ireplace(".", "", $data_get['t_luastanah']) * str_ireplace(".", "", $data_get['t_hargatanah']);
            $t_jmlhpajak = $t_luastanah * $data_get['t_tarifretribusi'];
        } elseif ($data_get['t_kategorisatu'] == 8 || $data_get['t_kategorisatu'] == 9) {
            $t_jmlhpajak = $data_get['t_jmlhbln'] * str_ireplace(".", "", $data_get['t_hargadasarsewa']);
        } elseif ($data_get['t_kategorisatu'] == 1 || $data_get['t_kategorisatu'] == 2 || $data_get['t_kategorisatu'] == 4 || $data_get['t_kategorisatu'] == 5 || $data_get['t_kategorisatu'] == 6 || $data_get['t_kategorisatu'] == 7) {
            $t_luastanah = $data_get['t_jmlhbln'] * str_ireplace(".", "", $data_get['t_luastanah']) * str_ireplace(".", "", $data_get['t_nilailuastanah']);
            $t_luasbangunan = $data_get['t_jmlhbln'] * str_ireplace(".", "", $data_get['t_luasbangunan']) * str_ireplace(".", "", $data_get['t_nilailuasbangunan']);
            $t_jmlhpajak = $t_luastanah + $t_luasbangunan;
        } else {
            $t_jmlhpajak = $data_get['t_jmlhbln'] * str_ireplace(".", "", $data_get['t_hargadasarsewa']);
        }


        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
//            "t_nilailuastanah" => number_format($datatarif['s_nilailuastanah'], 0, ",", "."),
//            "t_nilailuasbangunan" => number_format($datatarif['s_nilailuasbangunan'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    //hapus
    public function hapusAction() {
        /** Hapus Pendataan
         * @param int $s_idkorek
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $post = $req->getPost()->toArray();
        // echo '<pre>';
        // print_r($post['t_idtransaksi']);
        // print_r($post['s_idjenis']);
        // exit();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $this->Tools()->getService('PendataanTable')->hapusPendataan($post['t_idtransaksi']);
        $this->Tools()->getService('DetailreklameTable')->hapusdetailreklame($post['t_idtransaksi']);
        $this->Tools()->getService('DetailRetribusiTable')->hapusdetailretribusi($post['t_idtransaksi']);

        return $this->getResponse();
    }

    public function comboKelurahanCamatAction() {
        $frm = new \Pajak\Form\Pendaftaran\PendaftaranFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Pajak\Model\Pendaftaran\PendaftaranBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->Tools()->getService('PendaftaranTable')->getByKecamatan($ex);
                $opsi = "";
                $opsi .= "<option value=''>Silahkan Pilih</option>";
                foreach ($data as $r) {
                    $opsi .= "<option value='" . $r['s_idkel'] . "'>" . str_pad($r['s_kodekel'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_namakel'] . "</option>";
                }
                $res->setContent($opsi);
            }
        }
        return $res;
    }

    public function cetakkodebayarAction() {
        /** Cetak Kode Bayar
         * @param int $t_idwp
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 02/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
		// $this->registrasiva($data_get['t_idtransaksi']);
        // var_dump($this->registrasiva($data_get['t_idtransaksi'])); exit();
        
        $data = $this->Tools()->getService('PembayaranTable')->getDataPembayaranID($data_get['t_idtransaksi']);
        
        
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetakpendataanAction() {
        /** Cetak Pendataan
         * @param string $tglcetak Tanggal Mencetak Dokumen
         * @param date('d-m-Y') $tglpendataan0 Tanggal Minimal Pendataan
         * @param date('d-m-Y') $tglpendataan1 Tanggal Maximal Pendataan
         * @param int $t_kecamatan 
         * @param int $t_kelurahan  
         * @param int $t_idkorek
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataan($data_get->tglpendataan0, $data_get->tglpendataan1, $data_get->t_kecamatan, $data_get->t_kelurahan, $data_get->t_idkorek);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'tglpendataan0' => $data_get->tglpendataan0,
            'tglpendataan1' => $data_get->tglpendataan1,
            'tglcetak' => $data_get->tglcetak,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function dataPendataanAction() {
        /** Mendapatkan Data Pendataan
         * @param int $page
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 05/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
// Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        $data_render = array(
            "idtransaksi" => $data['t_idtransaksi'],
            "namawp" => $data['t_nama'],
            "alamatwp" => $data['t_alamat_lengkapwp'],
            "namaop" => $data['t_namaobjek'],
            "alamatop" => $data['t_alamatlengkapobjek'],
            "tglketetapan" => date('d-m-Y', strtotime($data['t_tglpendataan'])),
            "jenispajak" => $data['s_namajenis'],
            "idjenispajak" => $data['t_jenisobjek'],
            "t_idobjek" => $data['t_idobjek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataWPObjekAction() {
        /** Mendapatkan Data WP Objek
         * @param int $page
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 20/06/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataWPObjek($data_get['idobjek']);
        $data_render = array(
            "idobjek" => $data['t_idobjek'],
            "namawp" => $data['t_nama'],
            "alamatwp" => $data['t_alamat_lengkapwp'],
            "namaop" => $data['t_namaobjek'],
            "alamatop" => $data['t_alamatlengkapobjek'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetakkartudataAction() {
        /** Cetak Kartu Data
         * @param int $idobjek
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 20/06/2019
         */
         
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data Jenis Pajak
        $datajenispajak = $this->Tools()->getService('PendaftaranTable')->getDataJenisPajak($data_get['idobjek']);
        $korekboga = $this->Tools()->getService('PendaftaranTable')->getkorek($datajenispajak['t_korekobjek']);
        // Mengambil Data WP
        $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranIDObjek($data_get['idobjek']);
        
        // Mengambil Data Penetapan dan Pembayaran
        $dataarr = array();
        for ($i = 1; $i <= 12; $i++) {
        $datatransaksi = $this->Tools()->getService('PendaftaranTable')->getTransaksi($i, $data_get['periode'], $data_get['idobjek']);
            if ($datatransaksi == false) {
                $datatransaksi = array(
                    "t_tglpendataan" => null,
                    "t_jmlhpajak" => null,
                    "t_tglpembayaran" => null,
                    "t_jmlhpembayaran" => null
                );
            } else {
                $datatransaksi = $datatransaksi;
            }
            $dataarr[] = array_merge($datatransaksi);
        }

        $datatransaksireklame = $this->Tools()->getService('PendaftaranTable')->getTransaksireklame($data_get['periode'], $data_get['idobjek']);
        //////////////////////////////////////////////
            // echo '<pre>';
            // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($datatransaksireklame));
            // // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($datajenispajak));
            // // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($datatransaksi));
            // print_r($korekboga[0]['korek']);
            // print_r($data_get);
            // echo '</pre>';
            // exit();
        //////////////////////////////////////////////
         
        // utk boga

        
        // $datatransaksiair = $this->Tools()->getService('PendaftaranTable')->getTransaksiAir($data_get['periode'], $data_get['idobjek']);
        // $datatransaksiair = $this->Tools()->getService('PendaftaranTable')->getTransaksiAir($i, $data_get['periodekartudata'], $data_get['objekpajakwp']);
        $datatransaksiretribusi = $this->Tools()->getService('PendaftaranTable')->getTransaksiRetribusi($data_get['periode'], $data_get['idobjek']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_mengetahui = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttdmengetahui']);
        $ar_diperiksa = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttddiperiksa']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'korekboga' => $korekboga[0]['korek'],
            'datatransaksi' => $dataarr,
            'datatransaksireklame' => $datatransaksireklame,
            // 'datatransaksiair' => $datatransaksiair,
            'datatransaksiretribusi' => $datatransaksiretribusi,
            'ar_pemda' => $ar_pemda,
            'ar_mengetahui' => $ar_mengetahui,
            'ar_diperiksa' => $ar_diperiksa,
            "datajenispajak" => $datajenispajak,
            'periodepajak' => $data_get['periode']
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetaksptpdhotelAction() {
         
        /** Cetak SPTPD Hotel
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getOnlySubRekeningByJenis(1);
        // $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(1);

        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);

        //////////////////////////////////////////////
        // echo '<pre>';
        // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($rekening));
        // echo '</pre>';
        // exit();
        //////////////////////////////////////////////
         
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
            'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdrestoranAction() {    
  
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getOnlySubRekeningByJenis(2);
         
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);
        $pdf = new \LosPdf\View\Model\PdfModel();
         
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
            'operator' => $ar_operator   
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdhiburanAction() {
        /** Cetak SPTPD Hiburan
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getOnlySubRekeningByJenis(3);
        
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
            'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdreklameAction() {
        /** Cetak SPTPD Reklame
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        $datadetail = $this->Tools()->getService('DetailreklameTable')->getDetailReklameByIdTransaksi($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datadetail' => $datadetail,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "legal");
        return $pdf;
    }

    public function cetaksptpdppjAction() {

        $req = $this->getRequest();
        $data_get = $req->getQuery();    

        
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);    
        $detailppj = $this->Tools()->getService('DetailPpjTable')->getPendataanByIdTransaksi($data_get['idtransaksi']);
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        // $petugas_penerima = $this->Tools()->getService('PejabatTable')->getPejabatId($datasekarang['t_operatorpendataan']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);
         
        $pdf = new \LosPdf\View\Model\PdfModel();    
        $pdf->setVariables(array(
            'data' => $datasekarang,    
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'detailppj' => $detailppj,
            'ar_ttd' => $ar_ttd,
            // 'petugas_penerima' => $petugas_penerima,
            'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaklampiranppjAction() {
        /** Cetak SPTPD PPJ
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['t_idtransaksi']);
        $detailppjsekarang = $this->Tools()->getService('DetailPpjTable')->getPendataanByIdTransaksi($data_get['t_idtransaksi']);
        $lampiranppj_a = $this->Tools()->getService('LampiranPpjTable')->getLampiranByIdTransaksi($data_get['t_idtransaksi'], 1);
        $lampiranppj_b = $this->Tools()->getService('LampiranPpjTable')->getLampiranByIdTransaksi($data_get['t_idtransaksi'], 2);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'detailppj' => $detailppjsekarang,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'lampiranppj_a' => $lampiranppj_a,
            'lampiranppj_b' => $lampiranppj_b,
        ));
        $pdf->setOption("paperSize", "legal");
        return $pdf;
    }

    public function cetaksptpdminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil data Detail Minerba
        $datatarifrabminerba = $this->Tools()->getService('DetailminerbaTable')->getTarifRab();
        $datadetailrabminerba = $this->Tools()->getService('PendataanTable')->getDataDetailRabMinerba($data_get['idtransaksi']);
        $datadetailnonrabminerba = $this->Tools()->getService('PendataanTable')->getDataDetailNonRabMinerba($data_get['idtransaksi']);
        $datadetailminerba = $this->Tools()->getService('PendataanTable')->getDataDetailMinerba($data_get['idtransaksi']);

        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'dataminerba' => $datadetailminerba,
            'datarabminerba' => $datadetailrabminerba,
            'datanonrabminerba' => $datadetailnonrabminerba,
            'datatarifrabminerba' => $datatarifrabminerba,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdparkirAction() {
        /** Cetak SPTPD Parkir
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 31/01/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(7);
//// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
//// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
//// Mengambil Data Pemda
// Mengambil Data WP, OP dan Transaksi
        // $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        // $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil data Detail Minerba
        // $datadetailparkir = $this->Tools()->getService('PendataanTable')->getDataDetailParkir($data_get['idtransaksi']);

        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            // 'dataparkir' => $datadetailparkir,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
            'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdabtAction() {
        /** Cetak SPTPD ABT
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdwaletAction() {
        /** Cetak SPTPD Walet
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(9);
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,  
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
            'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function tentukanMasaAction() {
        /** tentukan Masa Pajak Akhir
         * @param int $t_masaawal
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 06/02/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $t_masaakhir = date('t', strtotime($data_get['t_masaawal'])) . '-' . date('m', strtotime($data_get['t_masaawal'])) . '-' . date('Y', strtotime($data_get['t_masaawal']));

        $data_render = array(
            "t_masaakhir" => $t_masaakhir
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungpajakAction() {
        /** Hitung Pajak Default
         * @param int $t_dasarpengenaan
         * @param int $t_tarifpajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 13/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($data_get['t_jenispajak'] == 2) { // restoran
//            if ($data_get['t_idkorek'] == 21) { // katering
//                $datarek = $this->Tools()->getService('RekeningTable')->getdataRekeningId($data_get['t_idkorek']);
//                $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
//                $t_tarifpajak = $datarek['s_persentarifkorek'];
//                $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
//            } else {
            $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
//            if ($t_dasarpengenaan < 3000000) {
//                $t_tarifpajak = 0;
//                $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
//            } else {
//                $t_tarifpajak = 10;
            $t_tarifpajak = $data_get['t_tarifpajak'];
            $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
//            }
//            }
            $t_jmlhkenaikan = 0;
            if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
                $t_jmlhkenaikan = ($t_jmlhpajak * $data_get['t_tarifkenaikan'] / 100);
                $t_jmlhpajak = $t_jmlhpajak + $t_jmlhkenaikan;
            }
        } else {
            $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
            $t_tarifpajak = $data_get['t_tarifpajak'];
            $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
            $t_jmlhkenaikan = 0;
            if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
                $t_jmlhkenaikan = ($t_jmlhpajak * $data_get['t_tarifkenaikan'] / 100);
                $t_jmlhpajak = $t_jmlhpajak + $t_jmlhkenaikan;
            }
        }
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", "."),
            "t_tarifpajak" => $t_tarifpajak
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungpajakreklameAction() {
        /** Hitung Pajak Reklame Pandeglang
         * @param int $t_dasarpengenaan
         * @param int $t_tarifpajak
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 17/01/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_reklame = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data_get->t_jenisreklame);
        
        $luas = ($data_get->t_panjang * $data_get->t_lebar);
        $luas_reklame = ($luas == 0) ? 1 : $luas;
        $nsr = $luas_reklame * $data_reklame['s_tarif'];

        $jumlahpajak = $data_get->t_sudutpandang * $data_get->t_masareklame * $data_get->t_jmlhreklame * $nsr * $data_reklame['s_persentarifkorek'] / 100;

        $data_render = array(
            "t_luas" => number_format($luas, 2, ".", "."),
            "t_nsr" => number_format($nsr, 0, ",", "."),
            "t_jmlhpajak" => number_format($jumlahpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    // public function hitungpajakreklameAction() {
    //     /** Hitung Pajak Reklame Pandeglang
    //      * @param int $t_dasarpengenaan
    //      * @param int $t_tarifpajak
    //      * @author Roni Mustapa <ronimustapa@gmail.com>
    //      * @date 17/01/2018
    //      */
    //     $req = $this->getRequest();
    //     $data_get = $req->getPost();
    //     // $data_reklame = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data_get->t_jenisreklame);
    //     // $tipewaktu = ($data_reklame['s_ket'] == 'Harian') ? '0' : '2';
        
    //     // $luas = ($data_get->t_panjang * $data_get->t_lebar);
    //     // $luas_reklame = ($luas == 0) ? 1 : $luas;
    //     // $nsr = $luas_reklame * $data_reklame['s_nsr'];
        
    //     // $jumlahpajak = $data_get->t_sudutpandang * $data_get->t_jangkawaktu * $data_get->t_jumlah * $nsr * $data_korek['s_persentarifkorek'] / 100;
        
        
    //     // reklame puncak jaya
    //     $data_jenisreklame = $this->Tools()->getService('ReklameTable')->getDataKorek($data_get['t_idkorek']);
    //     $data_jeniswpreklame = $this->Tools()->getService('ReklameTable')->getHargaReklame($data_get['t_jenisreklame']);
    //     // $jumlahpajak = $data_jeniswpreklame
    //     // end->edited ade

    //     // var_dump($data_jeniswpreklame); exit();
    //     $data_render = array(
    //         "t_jenisreklame" => $data_jenisreklame,
    //         "t_jmlhpajak" => number_format($data_jeniswpreklame['s_tarifreklame'], 0, ",", "."),
    //         "t_tarifpajak" => $data_jeniswpreklame['s_tarifreklame'],
    //     );
    //     return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    // }

    public function hitungpajakairAction() {
        /** Hitung Pajak Air
         * @param int $t_volume
         * @param int $t_tarifpajak
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 13/09/2017
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_volume = $data_get['t_volume'];
        $ar_tarif = $this->Tools()->getService('AirTable')->getDataId($data_get['t_peruntukan']);

        if($t_volume <= 50){
            $tarifAir = $ar_tarif->s_nilai1;
        }elseif($t_volume >= 51 && $t_volume <= 500){
            $tarifAir = $ar_tarif->s_nilai2;
        }elseif($t_volume >= 501 && $t_volume <= 1000){
            $tarifAir = $ar_tarif->s_nilai3;
        }elseif($t_volume >= 1001 && $t_volume <= 2500){
            $tarifAir = $ar_tarif->s_nilai4;
        }else{
            $tarifAir = $ar_tarif->s_nilai5;
        }

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_jmlhpajak * $data_get['t_tarifkenaikan'] / 100);
            $t_jmlhpajak = $t_jmlhpajak + $t_jmlhkenaikan;
        }

        $HDA = $tarifAir;
        $FNA = $t_volume * $HDA;
        $NPA = round($FNA);
        $pajak = $NPA * ($data_get['t_tarifpajak'] / 100);

        $data_render = array(
            "t_jmlhpajak" => number_format($pajak, 0, ',', '.'),
            "t_hargadasarair" => number_format($HDA, 0, ',', '.'),
            "t_totalnpa" => number_format($NPA, 0, ',', '.'),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", "."),
            'perhitungan' => " ( Rp. " . number_format($NPA, 0, ',', '.') . " (Total NPA)  x " . $data_get['t_tarifpajak'] . "% (Tarif Pajak) )",
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    /*
     * HITUNG RETRIBUSI
     * AUTHOR : RONI MUSTAPA
     * EMAIL : ronimustapa@gmail.com
     * created : 12/04/2019
     */

    public function hitungretribusiAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $data = array(
            't_jmlhpajak' => number_format($t_jumlah, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungjmlhretribusiAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalretribusiAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $data = array(
            't_jmlhpajak' => number_format($t_jumlah, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailkirTable')->getDataId($data_get['t_idtarif']);
        $data_render = array(
            "t_hargadasar" => number_format($data['s_tarif'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function caritarifparkirtepiAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailparkirtepiTable')->getDataIdParkirtepi($data_get['t_idtarif']);
        $data_render = array(
            "t_hargadasar" => number_format($data['s_tarif'], 0, ",", "."),
            "t_satuan" => $data['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusipengujiankendaraanbermotorAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlahkendaraan']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalretribusipengujiankendaraanbermotorAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $data = array(
            't_jmlhpajak' => number_format($t_jumlah, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungretribusiparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalretribusiparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']) + str_ireplace(".", "", $data_get['t_jumlah101']) + str_ireplace(".", "", $data_get['t_jumlah102']) + str_ireplace(".", "", $data_get['t_jumlah103']) + str_ireplace(".", "", $data_get['t_jumlah104']) + str_ireplace(".", "", $data_get['t_jumlah105']) + str_ireplace(".", "", $data_get['t_jumlah106']) + str_ireplace(".", "", $data_get['t_jumlah107']);
        $data = array(
            't_jmlhpajak' => number_format($t_jumlah, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifpemadamAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailpemadamTable')->getDataId($data_get['t_idtarif']);
        $data_render = array(
            "t_tarifdasar" => number_format($data['s_tarif'], 0, ",", "."),
            "t_satuan" => $data['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretpemadamAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jmltitikbuah']) * str_ireplace(".", "", $data_get['t_tarifdasar']);
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalretpemadamAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $data = array(
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifteraAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataTarif = $this->Tools()->getService('DetailteraTable')->getDataTeraId($data_get['t_idtarif']);
        $data = array(
            't_hargadasar' => number_format($dataTarif['s_tarif'], 0, ",", "."),
            't_satuan' => $dataTarif['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungretteraAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalretteraAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']) + str_ireplace(".", "", $data_get['t_jumlah8']) + str_ireplace(".", "", $data_get['t_jumlah8']) + str_ireplace(".", "", $data_get['t_jumlah9']) + str_ireplace(".", "", $data_get['t_jumlah10']);
        $data = array(
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritariftrayekAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailtrayekTable')->getDataId($data_get['t_idtarif']);
        $data_render = array(
            "t_hargadasar" => number_format($data['s_tarif'], 0, ",", "."),
            "t_satuan" => $data['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungrettrayekAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jmlhperjalanan']) * str_ireplace(".", "", $data_get['t_jmlhkendaraan']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function carijenispelayananterminalAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataTarif = $this->Tools()->getService('DetailterminalTable')->getdataTarifTerminal($data_get['t_idjenis']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataTarif as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idtarif'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function caritarifterminalAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailterminalTable')->getDataIdTerminal($data_get['t_idtarif']);
        $data_render = array(
            "t_hargadasar" => number_format($data['s_tarifdasar'], 0, ",", "."),
            "t_satuan" => $data['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cariKlasifikasiTarifKebersihanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataKlasifikasi = $this->Tools()->getService('DetailkebersihanTable')->getDataKlasifikasiKategori($data_get['t_klasifikasi']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataKlasifikasi as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idklasifikasi'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusikebersihanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailkebersihanTable')->getDataKlasifikasiTarif($data_get['t_kategori']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        $t_jmlhpajak = $data_get['t_jmlhbln'] * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
            "t_satuan" => $datatarif['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cariKlasifikasiTarifPasarAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataKlasifikasi = $this->Tools()->getService('DetailpasarTable')->getDataKlasifikasiKategori($data_get['t_klasifikasi']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataKlasifikasi as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idklasifikasi'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusipasarAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailpasarTable')->getDataKlasifikasiTarif($data_get['t_jenisbangunan']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        if ($data_get['t_jenisbangunan'] == 10) {
            $t_jmlhpajak = $datatarif['s_tarifdasar'];
        } else {
            $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
            $t_jmlhpajak = $data_get['t_jmlhbln'] * $t_luas * $datatarif['s_tarifdasar'];
        }
        $data_render = array(
            "t_jenisbangunan" => $data_get['t_jenisbangunan'],
            "t_tipewaktu" => $datatarif['s_satuan'],
            "t_luas" => $t_luas,
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cariKlasifikasiTarifPasargrosirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataKlasifikasi = $this->Tools()->getService('DetailpasargrosirTable')->getDataKlasifikasiKategori($data_get['t_klasifikasi']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataKlasifikasi as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idklasifikasi'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusipasargrosirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailpasargrosirTable')->getDataKlasifikasiTarif($data_get['t_jenisbangunan']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_jmlhpajak = $data_get['t_jmlhbln'] * $t_luas * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_tipewaktu" => $datatarif['s_satuan'],
            "t_luas" => $t_luas,
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function carikategorigolonganparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailtempatparkirTable')->getDataJeniskendaraan($data_get['t_idkorek']);
        $opsi = "";
        $opsi .= "<option value=''>-- SILAHKAN PILIH --</option>";
        foreach ($data as $v) {
            $opsi .= "<option value='" . $v['s_idtarif'] . "'>" . $v['s_idtarif'] . " || " . $v['s_jeniskendaraan'] . "</option>";
        }
        $data_render = array(
            "t_jeniskendaraan" => $opsi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusitempatparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailtempatparkirTable')->getDataTarif($data_get['t_jeniskendaraan']);
        $t_jmlhpajak = $data_get['t_jmlhkendaraan'] * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusirumahdinasAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $datatarif = $this->Tools()->getService('RumahdinasTable')->getDataRange($t_luas);
        $t_jmlhpajak = ($t_luas * $datatarif['s_tarif'] * $data_get['t_jmlhbln']) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarif'], 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretpangrekAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_tarifdasar = str_ireplace(".", "", $data_get['t_tarifdasar']);
        $t_jmlhpajak = ($t_luas * $t_tarifdasar * $data_get['t_jmlhbln']) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungrettanahlainAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_tarifdasar = str_ireplace(".", "", $data_get['t_tarifdasar']);
        $t_jmlhpajak = ($t_luas * $t_tarifdasar * $data_get['t_jmlhbln']) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungrettanrekAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_jmlhblnhari = $data_get['t_jmlhblnhari'];
        $t_tarifdasar = (int) str_ireplace(".", "", $data_get['t_tarifdasar']);
        $t_jmlhpajak = ($t_luas * $t_tarifdasar * $t_jmlhblnhari) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendaftaranByObjekAction() {
        /** Cari Transaksi By Objek Pajak
         * @param int $npwpd
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
// Mengambil Data WP, OP dan Transaksi
        $op = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjek']);
        $dataop = " <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>NPWPD</label>
                        <div class='col-sm-10'>
                            : " . $op['t_npwpd'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Nama WP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_nama'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Alamat WP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_alamat'] . "
                            ,Desa/Kel. " . $op['s_namakel'] . "
                            ,Kec. " . $op['s_namakec'] . "
                            ,Kab. " . $op['t_kabupaten'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>NIOP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_nop'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Nama OP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_namaobjek'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Alamat OP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_alamatobjek'] . "
                            ,Desa/Kel. " . $op['s_namakelobjek'] . "
                            ,Kec. " . $op['s_namakecobjek'] . "
                            ,Kab. " . $op['t_kabupatenobjek'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2 control-label'>Periode Pajak</label>
                        <div class='col-sm-2'>
                            <input type='text' class='form-control' name='periodepajak' id='periodepajak'>
                        </div>
                        <div class='col-sm-1'>
                            <input type='button' class='btn btn-sm btn-primary' value='Cari' onclick='CariPendataanByObjek(" . $op['t_idobjek'] . ");'>
                        </div>
                    </div>";
        $datatransaksi = "";
        $data_render = array(
            "dataop" => $dataop,
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanByObjekAction() {
        //////////////////////////////////////////////
        // echo '<pre>';
        // print_r('gegel');
        // echo '</pre>';
        // exit();
        //////////////////////////////////////////////
         
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $abulan = ['1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
//        $tombolTeguranDanSKPDJ = "<td data-title='#' style='text-align:right'><a href='javascript:void(0)' class='btn btn-primary btn-xs' onclick='bukaCetakSuratTeguran($dataparameterTeguran, $data_get[idobjek])'><i class='glyph-icon icon-print'></i> Surat Teguran</href> <a href='javascript:void(0)' class='btn btn-warning btn-xs' onclick='pilihskpdjabatan($dataparameter)'><i class='glyph-icon icon-hand-o-up'></i> SKPD Jabatan</href></td>";
        if ($dataawal['t_idkorek'] == 25) {//katering
            $rowKatering = $this->Tools()->getService('ObjekTable')->getPendataanKatering($data_get['periodepajak'], $data_get['idobjek']);
            $i = 1;
            foreach ($rowKatering as $row) {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i++] . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                    
                                            </tr>";
            }     
        } else {
            for ($i = 1; $i <= 12; $i++) {
                $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
                if ($row == false) {
//                $today = (int) date('m');
                    $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                    if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                        $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $datatransaksi .= "     <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'><b style='color:red'>Belum Melakukan Pelaporan Pajak</b></td>
                                                <td data-title='Tgl' style='text-align:center'></td>
                                                <td data-title='Masa Pajak' style='text-align:center'></td>
                                                <td data-title='Pajak' style='text-align:right'></td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='#' style='text-align:right'>
                                                <a href='javascript:void(0)' class='btn btn-warning btn-xs' onclick='pilihskpdjabatan($dataparameter)'><i class='glyph-icon icon-hand-o-up'></i> SKPD Jabatan</href></td>
                                                </tr>";
                        //<a href='javascript:void(0)' class='btn btn-primary btn-xs' onclick='bukaCetakSuratTeguran($dataparameterTeguran, $data_get[idobjek])'><i class='glyph-icon icon-print'></i> Surat Teguran</href> 
                    } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                        $datatransaksi .= "     <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                    }
                } else {
                    $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                    $datatransaksi .= "         <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                            </tr>";
                }
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanByObjekOfficialAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
        if ($dataawal['t_idkorek'] == 25) {//katering
            $rowKatering = $this->Tools()->getService('ObjekTable')->getPendataanKatering($data_get['periodepajak'], $data_get['idobjek']);
            $i = 1;
            foreach ($rowKatering as $row) {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                    
                                            </tr>";
            }
        } else {
            for ($i = 1; $i <= 12; $i++) {
                $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
                if ($row == false) {
//                $today = (int) date('m');
                    $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                    if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                        $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'><b style='color:red'>Belum dilakukan pendataan</b></td>
                                                <td data-title='Tgl' style='text-align:center'></td>
                                                <td data-title='Masa Pajak' style='text-align:center'></td>
                                                <td data-title='Pajak' style='text-align:right'></td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $dataawal['s_jenisobjek'] . "</td>";

                        $datatransaksi .= "<td data-title='#' style='text-align:center'></td>";

                        $datatransaksi .= "</tr>";
                    } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                        $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                    }
                } else {
                    $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                    $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                            </tr>";
                }
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanReklameByObjekAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataHistory = $this->Tools()->getService('ObjekTable')->getHistoryReklame($data_get['idobjek']);
        $i = 1;
        foreach ($dataHistory as $row) {
            $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-'); // returns true
            $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
            $datatransaksi .= " <tr>
                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                <td data-title='Kode Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                            </tr>";
        }

        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    // case added function
    public function CariHistoryTransaksiTanpaMasaAction() {
         
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";

        $dataHistory = $this->Tools()->getService('ObjekTable')->getHistoryTidakBerdasarkanMasaPertahun($data_get['idobjek'], $data_get['periodepajak']);
       
        $i = 1;
        foreach ($dataHistory as $row) {
            $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-'); // returns true
            $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
            $datatransaksi .= " <tr>
                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                <td data-title='Kode Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                            </tr>";
        }

        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
    // case added function
    public function CariPendataanKateringByObjekAction() {

        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataHistory = $this->Tools()->getService('ObjekTable')->getHistoryTidakBerdasarkanMasa($data_get['idobjek']);
        $i = 1;
        foreach ($dataHistory as $row) {
            $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-'); // returns true
            $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
            $datatransaksi .= " <tr>
                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                <td data-title='Kode Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                            </tr>";
        }

        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    
    public function CariPendataanMinerbaByObjekAction() {

        $req = $this->getRequest();
        $data_get = $req->getPost();
         
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataHistory = $this->Tools()->getService('ObjekTable')->getHistoryTidakBerdasarkanMasaPertahun($data_get['idobjek'], $data_get['periodepajak']);
                 
        $i = 1;
        foreach ($dataHistory as $row) {
            $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-'); // returns true
            $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
            $datatransaksi .= " <tr>
                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                <td data-title='Kode Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                            </tr>";
        }

        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function getgolonganberdasarkanklasifikasiAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
         
        $golongan = $this->Tools()->getService('ObjekTable')->getgolonganberdasarkanklasifikasi($data_get['t_klasifikasi']);
         
        $data_render = array(
            "golongan" => $golongan
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanPpjByObjekAction() {
         
        $req = $this->getRequest();
        $data_get = $req->getPost();
         
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataHistory = $this->Tools()->getService('ObjekTable')->getHistoryTidakBerdasarkanMasaPertahun($data_get['idobjek'], $data_get['periodepajak']);
                 
        $i = 1;
        foreach ($dataHistory as $row) {
            $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-'); // returns true
            $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
            $datatransaksi .= " <tr>
                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                <td data-title='Kode Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                            </tr>";
        }

        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanAirByObjekAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
        for ($i = 1; $i <= 12; $i++) {
            $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
            if ($row == false) {
                $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                    $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                    $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01" . str_pad($data_get['idobjek'], 9, '0', STR_PAD_LEFT);
                    $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'><b style='color:red'>Belum Melakukan Penetapan</b></td>
                                                <td data-title='Tgl' style='text-align:center'></td>
                                                <td data-title='Masa Pajak' style='text-align:center'></td>
                                                <td data-title='Pajak' style='text-align:right'></td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'></td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'></td>
                                                <td data-title='#' style='text-align:right'><a href='javascript:void(0)' class='btn btn-warning btn-xs' onclick='pilihskpdjabatan($dataparameter)'><i class='glyph-icon icon-hand-o-up'></i> SKPD Jabatan</href></td>
                                            </tr>";
                } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                    $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                }
            } else {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpenetapan'])) . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                            </tr>";
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridRekeningAction() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;   
        $limit = $base->rows;
        $count = $this->Tools()->getService('RekeningTable')->getGridCountRekening($base, $parametercari);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $s = "";

        $data = $this->Tools()->getService('RekeningTable')->getGridDataRekening($base, $start, $parametercari);
        if($count == 0){
            $s .= "<tr><td colspan='5' style='color:#ccc;'>Tidak Ada Data</td></tr>";
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['korek'] . "</td>";
            $s .= "<td>" . $row['s_namakorek'] . "</td>";
            $s .= "<td>" . $row['s_persentarifkorek'] . "</td>";
            $s .= "<td><a href='#' onclick='pilihRekening(" . $row['s_idkorek'] . ");return false;' class='btn btn-sm btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function pilihRekeningAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['s_idkorek']);
        if ($dataRekening['t_berdasarmasa'] == 'Yes') {
            $t_berdasarmasa = "Berdasar Masa";
        } else {
            $t_berdasarmasa = "Tidak Berdasar Masa";
        }
        $opsi = "";
        if ($dataRekening['s_jenisobjek'] == 4) {
            $dataReklame = $this->Tools()->getService('ReklameTable')->getDataReklameByIdRekening($data_get['s_idkorek']);
            $opsi .= "<option value=''>Silahkan Pilih</option>";
            foreach ($dataReklame as $row) {
                $opsi .= "<option value='" . $row['s_kodejenis'] . "'>" . $row['s_namareklame'] . "</option>";
            }
        }
        $data = array(
            't_idkorek' => $dataRekening['s_idkorek'],
            't_korek' => $dataRekening['korek'],
            't_namakorek' => strtoupper($dataRekening['s_namakorek']),
            't_tarifpajak' => $dataRekening['s_persentarifkorek'],
            't_tarifdasarkorek' => number_format($dataRekening['s_tarifdasarkorek'], 0, ',', '.'),
            // 't_tarifdasarkorek' => $dataRekening['s_tarifdasarkorek'],
            't_jenisreklame' => $opsi,
            't_berdasarmasa' => $t_berdasarmasa,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
    
    public function dataGridRabminerbaAction() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;   
        $limit = $base->rows;
        $count = $this->Tools()->getService('RekeningTable')->getGridCountRab($base, $parametercari);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        $data = $this->Tools()->getService('RekeningTable')->getGridDataRab($base, $start, $parametercari);
        if($count == 0){
            $s .= "<tr><td colspan='5' style='color:#ccc;'>Tidak Ada Data</td></tr>";
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td style='text-align:center;'>" . $counter . "</td>";
            $s .= "<td>" . $row['s_jenispekerjaan'] . "</td>";
            $s .= "<td style='text-align:center;'><a href='#' onclick='pilihRab(" . $row['s_idrab'] . "," . $parametercari->inderab . ");return false;' class='btn btn-sm btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></td>";
            $s .= "</tr>";
            $counter++;
        }
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function pilihRabminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRab = $this->Tools()->getService('DetailminerbaTable')->getIndexRabById($data_get['idrab']);
        
        $data = array(
            't_idrab' => $dataRab['s_idrab'],
            't_jenisrab' => $dataRab['s_jenispekerjaan'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifnonrabminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $ar_tarif = $this->Tools()->getService('DetailminerbaTable')->getTarifIndexNonRabById($data_get->t_idnonrab);
        
        if(!empty($data_get->t_volumenonrab)){
            $t_indexnonrab = $ar_tarif['s_index'];
            $t_jumlahnonrab = $data_get->t_volumenonrab * $ar_tarif['s_index'];
            $t_tarifindexnonrab = $ar_tarif['s_hargadasar'];
            $t_pajaknonrab = $t_jumlahnonrab * $t_tarifindexnonrab;
        }else{
            $t_indexnonrab = 0;
            $t_jumlahnonrab = 0;
            $t_tarifindexnonrab = 0;
            $t_pajaknonrab = 0;
        }
        
        $data = array(
            't_indexnonrab' => number_format($t_indexnonrab, 2, ",", "."),
            't_jumlahnonrab' => number_format($t_jumlahnonrab, 2, ",", "."),
            't_tarifindexnonrab' => number_format($t_tarifindexnonrab, 2, ",", "."),
            't_pajaknonrab' => number_format($t_pajaknonrab, 2, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
    
    public function caritarifminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['t_idkorek']);
        // var_dump($dataRekening);exit();
        $data = array(
            't_hargapasaran' => number_format($dataRekening['s_tarifdasarkorek'], 0, ",", "."),
            't_tarifpersen' => $dataRekening['s_persentarifkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // var_dump($data_get);exit();
        // var_dump(str_ireplace(",", ".", $data_get['t_volume'])); exit();
        $t_jumlah = str_ireplace(",", ".", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargapasaran']);
        $t_pajak = $t_jumlah * $data_get['t_tarifpersen'] / 100;
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            't_pajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
    
    public function hitungpajakrabminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataIndex = $this->Tools()->getService('DetailminerbaTable')->getIndexRabById($data_get['t_idrab']);
        $dataTarif = $this->Tools()->getService('DetailminerbaTable')->getTarifRab();
        
        $batupecah = ($dataIndex['s_batupecah']*$dataTarif['s_batupecah'])*$data_get['t_volumerab'];
        $batublok = ($dataIndex['s_batublok']*$dataTarif['s_batublok'])*$data_get['t_volumerab'];
        $tanahurug = ($dataIndex['s_tanahurug']*$dataTarif['s_tanahurug'])*$data_get['t_volumerab'];
        $sirbang = ($dataIndex['s_sirbang']*$dataTarif['s_sirbang'])*$data_get['t_volumerab'];
        $sirug = ($dataIndex['s_sirug']*$dataTarif['s_sirug'])*$data_get['t_volumerab'];
        $grosok = ($dataIndex['s_grosok']*$dataTarif['s_grosok'])*$data_get['t_volumerab'];
        $sirtu = ($dataIndex['s_sirtu']*$dataTarif['s_sirtu'])*$data_get['t_volumerab'];
        $kapurpasang = ($dataIndex['s_kapurpasang']*$dataTarif['s_kapurpasang'])*$data_get['t_volumerab'];
        $tanahliat = ($dataIndex['s_tanahliat']*$dataTarif['s_tanahliat'])*$data_get['t_volumerab'];
        
        $data = array(
            't_batupecah' => number_format($batupecah, 2, ",", "."),
            't_batublok' => number_format($batublok, 2, ",", "."),
            't_tanahurug' => number_format($tanahurug, 2, ",", "."),
            't_sirbang' => number_format($sirbang, 2, ",", "."),
            't_sirug' => number_format($sirug, 2, ",", "."),
            't_grosok' => number_format($grosok, 2, ",", "."),
            't_sirtu' => number_format($sirtu, 2, ",", "."),
            't_kapurpasang' => number_format($kapurpasang, 2, ",", "."),
            't_tanahliat' => number_format($tanahliat, 2, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
	
    function pecahanDecimal($float){
        $res = str_ireplace(",", ".", str_ireplace(".", "", $float));
        return $res;
    }
    
    public function hitungjumlahindexminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataTarif = $this->Tools()->getService('DetailminerbaTable')->getTarifRab();
        
        $jmlhbatupecah = $this->pecahanDecimal($data_get['batupecah1'])+$this->pecahanDecimal($data_get['batupecah2'])+$this->pecahanDecimal($data_get['batupecah3'])+$this->pecahanDecimal($data_get['batupecah4'])+$this->pecahanDecimal($data_get['batupecah5'])+$this->pecahanDecimal($data_get['batupecah6'])+$this->pecahanDecimal($data_get['batupecah7'])+$this->pecahanDecimal($data_get['batupecah8'])+$this->pecahanDecimal($data_get['batupecah9'])+$this->pecahanDecimal($data_get['batupecah10'])
			+$this->pecahanDecimal($data_get['batupecah11'])+$this->pecahanDecimal($data_get['batupecah12'])+$this->pecahanDecimal($data_get['batupecah13'])+$this->pecahanDecimal($data_get['batupecah14'])+$this->pecahanDecimal($data_get['batupecah15'])+$this->pecahanDecimal($data_get['batupecah16'])+$this->pecahanDecimal($data_get['batupecah17'])+$this->pecahanDecimal($data_get['batupecah18'])+$this->pecahanDecimal($data_get['batupecah19'])+$this->pecahanDecimal($data_get['batupecah20']);
        $jmlhdasar_batupecah = $jmlhbatupecah * $dataTarif['s_harga_batupecah'];
        $jmlhbatublok = $this->pecahanDecimal($data_get['batublok1'])+$this->pecahanDecimal($data_get['batublok2'])+$this->pecahanDecimal($data_get['batublok3'])+$this->pecahanDecimal($data_get['batublok4'])+$this->pecahanDecimal($data_get['batublok5'])+$this->pecahanDecimal($data_get['batublok6'])+$this->pecahanDecimal($data_get['batublok7'])+$this->pecahanDecimal($data_get['batublok8'])+$this->pecahanDecimal($data_get['batublok9'])+$this->pecahanDecimal($data_get['batublok10'])
			+$this->pecahanDecimal($data_get['batublok11'])+$this->pecahanDecimal($data_get['batublok12'])+$this->pecahanDecimal($data_get['batublok13'])+$this->pecahanDecimal($data_get['batublok14'])+$this->pecahanDecimal($data_get['batublok15'])+$this->pecahanDecimal($data_get['batublok16'])+$this->pecahanDecimal($data_get['batublok17'])+$this->pecahanDecimal($data_get['batublok18'])+$this->pecahanDecimal($data_get['batublok19'])+$this->pecahanDecimal($data_get['batublok20']);
        $jmlhdasar_batublok = $jmlhbatublok * $dataTarif['s_harga_batublok'];
        $jmlhtanahurug = $this->pecahanDecimal($data_get['tanahurug1'])+$this->pecahanDecimal($data_get['tanahurug2'])+$this->pecahanDecimal($data_get['tanahurug3'])+$this->pecahanDecimal($data_get['tanahurug4'])+$this->pecahanDecimal($data_get['tanahurug5'])+$this->pecahanDecimal($data_get['tanahurug6'])+$this->pecahanDecimal($data_get['tanahurug7'])+$this->pecahanDecimal($data_get['tanahurug8'])+$this->pecahanDecimal($data_get['tanahurug9'])+$this->pecahanDecimal($data_get['tanahurug10'])
			+$this->pecahanDecimal($data_get['tanahurug11'])+$this->pecahanDecimal($data_get['tanahurug12'])+$this->pecahanDecimal($data_get['tanahurug13'])+$this->pecahanDecimal($data_get['tanahurug14'])+$this->pecahanDecimal($data_get['tanahurug15'])+$this->pecahanDecimal($data_get['tanahurug16'])+$this->pecahanDecimal($data_get['tanahurug17'])+$this->pecahanDecimal($data_get['tanahurug18'])+$this->pecahanDecimal($data_get['tanahurug19'])+$this->pecahanDecimal($data_get['tanahurug20']);
        $jmlhdasar_tanahurug = $jmlhtanahurug * $dataTarif['s_harga_tanahurug'];
        $jmlhsirbang = $this->pecahanDecimal($data_get['sirbang1'])+$this->pecahanDecimal($data_get['sirbang2'])+$this->pecahanDecimal($data_get['sirbang3'])+$this->pecahanDecimal($data_get['sirbang4'])+$this->pecahanDecimal($data_get['sirbang5'])+$this->pecahanDecimal($data_get['sirbang6'])+$this->pecahanDecimal($data_get['sirbang7'])+$this->pecahanDecimal($data_get['sirbang8'])+$this->pecahanDecimal($data_get['sirbang9'])+$this->pecahanDecimal($data_get['sirbang10'])
			+$this->pecahanDecimal($data_get['sirbang11'])+$this->pecahanDecimal($data_get['sirbang12'])+$this->pecahanDecimal($data_get['sirbang13'])+$this->pecahanDecimal($data_get['sirbang14'])+$this->pecahanDecimal($data_get['sirbang15'])+$this->pecahanDecimal($data_get['sirbang16'])+$this->pecahanDecimal($data_get['sirbang17'])+$this->pecahanDecimal($data_get['sirbang18'])+$this->pecahanDecimal($data_get['sirbang19'])+$this->pecahanDecimal($data_get['sirbang20']);
        $jmlhdasar_sirbang = $jmlhsirbang * $dataTarif['s_harga_sirbang'];
        $jmlhsirug = $this->pecahanDecimal($data_get['sirug1'])+$this->pecahanDecimal($data_get['sirug2'])+$this->pecahanDecimal($data_get['sirug3'])+$this->pecahanDecimal($data_get['sirug4'])+$this->pecahanDecimal($data_get['sirug5'])+$this->pecahanDecimal($data_get['sirug6'])+$this->pecahanDecimal($data_get['sirug7'])+$this->pecahanDecimal($data_get['sirug8'])+$this->pecahanDecimal($data_get['sirug9'])+$this->pecahanDecimal($data_get['sirug10'])
			+$this->pecahanDecimal($data_get['sirug11'])+$this->pecahanDecimal($data_get['sirug12'])+$this->pecahanDecimal($data_get['sirug13'])+$this->pecahanDecimal($data_get['sirug14'])+$this->pecahanDecimal($data_get['sirug15'])+$this->pecahanDecimal($data_get['sirug16'])+$this->pecahanDecimal($data_get['sirug17'])+$this->pecahanDecimal($data_get['sirug18'])+$this->pecahanDecimal($data_get['sirug19'])+$this->pecahanDecimal($data_get['sirug20']);
        $jmlhdasar_sirug = $jmlhsirug * $dataTarif['s_harga_sirug'];
        $jmlhgrosok = $this->pecahanDecimal($data_get['grosok1'])+$this->pecahanDecimal($data_get['grosok2'])+$this->pecahanDecimal($data_get['grosok3'])+$this->pecahanDecimal($data_get['grosok4'])+$this->pecahanDecimal($data_get['grosok5'])+$this->pecahanDecimal($data_get['grosok6'])+$this->pecahanDecimal($data_get['grosok7'])+$this->pecahanDecimal($data_get['grosok8'])+$this->pecahanDecimal($data_get['grosok9'])+$this->pecahanDecimal($data_get['grosok10'])
			+$this->pecahanDecimal($data_get['grosok11'])+$this->pecahanDecimal($data_get['grosok12'])+$this->pecahanDecimal($data_get['grosok13'])+$this->pecahanDecimal($data_get['grosok14'])+$this->pecahanDecimal($data_get['grosok15'])+$this->pecahanDecimal($data_get['grosok16'])+$this->pecahanDecimal($data_get['grosok17'])+$this->pecahanDecimal($data_get['grosok18'])+$this->pecahanDecimal($data_get['grosok19'])+$this->pecahanDecimal($data_get['grosok20']);
        $jmlhdasar_grosok = $jmlhgrosok * $dataTarif['s_harga_grosok'];
        $jmlhsirtu = $this->pecahanDecimal($data_get['sirtu1'])+$this->pecahanDecimal($data_get['sirtu2'])+$this->pecahanDecimal($data_get['sirtu3'])+$this->pecahanDecimal($data_get['sirtu4'])+$this->pecahanDecimal($data_get['sirtu5'])+$this->pecahanDecimal($data_get['sirtu6'])+$this->pecahanDecimal($data_get['sirtu7'])+$this->pecahanDecimal($data_get['sirtu8'])+$this->pecahanDecimal($data_get['sirtu9'])+$this->pecahanDecimal($data_get['sirtu10'])
			+$this->pecahanDecimal($data_get['sirtu11'])+$this->pecahanDecimal($data_get['sirtu12'])+$this->pecahanDecimal($data_get['sirtu13'])+$this->pecahanDecimal($data_get['sirtu14'])+$this->pecahanDecimal($data_get['sirtu15'])+$this->pecahanDecimal($data_get['sirtu16'])+$this->pecahanDecimal($data_get['sirtu17'])+$this->pecahanDecimal($data_get['sirtu18'])+$this->pecahanDecimal($data_get['sirtu19'])+$this->pecahanDecimal($data_get['sirtu20']);
        $jmlhdasar_sirtu = $jmlhsirtu * $dataTarif['s_harga_sirtu'];
        $jmlhkapurpasang = $this->pecahanDecimal($data_get['kapurpasang1'])+$this->pecahanDecimal($data_get['kapurpasang2'])+$this->pecahanDecimal($data_get['kapurpasang3'])+$this->pecahanDecimal($data_get['kapurpasang4'])+$this->pecahanDecimal($data_get['kapurpasang5'])+$this->pecahanDecimal($data_get['kapurpasang6'])+$this->pecahanDecimal($data_get['kapurpasang7'])+$this->pecahanDecimal($data_get['kapurpasang8'])+$this->pecahanDecimal($data_get['kapurpasang9'])+$this->pecahanDecimal($data_get['kapurpasang10'])
			+$this->pecahanDecimal($data_get['kapurpasang11'])+$this->pecahanDecimal($data_get['kapurpasang12'])+$this->pecahanDecimal($data_get['kapurpasang13'])+$this->pecahanDecimal($data_get['kapurpasang14'])+$this->pecahanDecimal($data_get['kapurpasang15'])+$this->pecahanDecimal($data_get['kapurpasang16'])+$this->pecahanDecimal($data_get['kapurpasang17'])+$this->pecahanDecimal($data_get['kapurpasang18'])+$this->pecahanDecimal($data_get['kapurpasang19'])+$this->pecahanDecimal($data_get['kapurpasang20']);
        $jmlhdasar_kapurpasang = $jmlhkapurpasang * $dataTarif['s_harga_kapurpasang'];
        $jmlhtanahliat = $this->pecahanDecimal($data_get['tanahliat1'])+$this->pecahanDecimal($data_get['tanahliat2'])+$this->pecahanDecimal($data_get['tanahliat3'])+$this->pecahanDecimal($data_get['tanahliat4'])+$this->pecahanDecimal($data_get['tanahliat5'])+$this->pecahanDecimal($data_get['tanahliat6'])+$this->pecahanDecimal($data_get['tanahliat7'])+$this->pecahanDecimal($data_get['tanahliat8'])+$this->pecahanDecimal($data_get['tanahliat9'])+$this->pecahanDecimal($data_get['tanahliat10'])
			+$this->pecahanDecimal($data_get['tanahliat11'])+$this->pecahanDecimal($data_get['tanahliat12'])+$this->pecahanDecimal($data_get['tanahliat13'])+$this->pecahanDecimal($data_get['tanahliat14'])+$this->pecahanDecimal($data_get['tanahliat15'])+$this->pecahanDecimal($data_get['tanahliat16'])+$this->pecahanDecimal($data_get['tanahliat17'])+$this->pecahanDecimal($data_get['tanahliat18'])+$this->pecahanDecimal($data_get['tanahliat19'])+$this->pecahanDecimal($data_get['tanahliat20']);
        $jmlhdasar_tanahliat = $jmlhtanahliat * $dataTarif['s_harga_tanahliat'];
        
        $jmlhpajak = $jmlhdasar_batupecah+$jmlhdasar_batublok+$jmlhdasar_tanahurug+$jmlhdasar_sirbang+$jmlhdasar_sirug+$jmlhdasar_grosok+$jmlhdasar_sirtu+$jmlhdasar_kapurpasang+$jmlhdasar_tanahliat;
        
        $data = array(
            't_jmlhbatupecah' => number_format($jmlhbatupecah, 2, ",", "."),
            't_jmlhdasar_batupecah' => number_format($jmlhdasar_batupecah, 2, ",", "."),
            't_jmlhbatublok' => number_format($jmlhbatublok, 2, ",", "."),
            't_jmlhdasar_batublok' => number_format($jmlhdasar_batublok, 2, ",", "."),
            't_jmlhtanahurug' => number_format($jmlhtanahurug, 2, ",", "."),
            't_jmlhdasar_tanahurug' => number_format($jmlhdasar_tanahurug, 2, ",", "."),
            't_jmlhsirbang' => number_format($jmlhsirbang, 2, ",", "."),
            't_jmlhdasar_sirbang' => number_format($jmlhdasar_sirbang, 2, ",", "."),
            't_jmlhsirug' => number_format($jmlhsirug, 2, ",", "."),
            't_jmlhdasar_sirug' => number_format($jmlhdasar_sirug, 2, ",", "."),
            't_jmlhgrosok' => number_format($jmlhgrosok, 2, ",", "."),
            't_jmlhdasar_grosok' => number_format($jmlhdasar_grosok, 2, ",", "."),
            't_jmlhsirtu' => number_format($jmlhsirtu, 2, ",", "."),
            't_jmlhdasar_sirtu' => number_format($jmlhdasar_sirtu, 2, ",", "."),
            't_jmlhkapurpasang' => number_format($jmlhkapurpasang, 2, ",", "."),
            't_jmlhdasar_kapurpasang' => number_format($jmlhdasar_kapurpasang, 2, ",", "."),
            't_jmlhtanahliat' => number_format($jmlhtanahliat, 2, ",", "."),
            't_jmlhdasar_tanahliat' => number_format($jmlhdasar_tanahliat, 2, ",", "."),
            't_jmlhpajak' => number_format(round($jmlhpajak), 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajaknonrabminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jmlhpajak = str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak1'])) + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak2'])) + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak3'])) 
                + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak4'])) + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak5'])) + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak6']))
                + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak7'])) + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak8'])) + str_ireplace(",", ".", str_ireplace(".", "", $data_get['t_pajak9']));
        $t_pajak = $t_jmlhpajak;
        
        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        
        $data = array(
            't_dasarpengenaan' => number_format($t_jmlhpajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", "."),
            't_jmlhpajak' => number_format(round($t_pajak), 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
    
    public function hitungtotalpajakminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']) + str_ireplace(".", "", $data_get['t_jumlah8']) + str_ireplace(".", "", $data_get['t_jumlah9']) + str_ireplace(".", "", $data_get['t_jumlah10']);
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7']) + str_ireplace(".", "", $data_get['t_pajak8']) + str_ireplace(".", "", $data_get['t_pajak9']) + str_ireplace(".", "", $data_get['t_pajak10']);
        $t_pajak = $t_jmlhpajak;

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            // 't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", ".")

            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakppjActionold() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah0 = str_ireplace(".", "", $data_get['nilailistrik0']);
        $t_jumlah1 = str_ireplace(".", "", $data_get['nilailistrik1']);
        $t_subpajak0 = round($t_jumlah0 * $data_get['persentarifkorek0'] / 100);
        $t_subpajak1 = round($t_jumlah1 * $data_get['persentarifkorek1'] / 100);
        $data = array(
            // 't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            'subtotalpajak0' => number_format($t_subpajak0, 0, ",", "."),
            'subtotalpajak1' => number_format($t_subpajak1, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakppjOldAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        if ($data_get['t_asallistrik'] == 2) {
            $tarif_pajak = 1.5;
            $t_subtotalkwh0 = str_ireplace(".", "", $data_get['subtotalkwh0']);
            $t_subtotalkwh1 = str_ireplace(".", "", $data_get['subtotalkwh1']);
            $t_jmlhpajak = ($t_subtotalkwh0 + $t_subtotalkwh1) * 1.5 / 100;
        } else {
            $tarif_pajak = 0;
            $t_subtotalpajak0 = str_ireplace(".", "", $data_get['subtotalpajak0']);
            $t_subtotalpajak1 = str_ireplace(".", "", $data_get['subtotalpajak1']);
            $t_jmlhpajak = ($t_subtotalpajak0 + $t_subtotalpajak1);
        }
        // $t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7']);
        $t_pajak = $t_jmlhpajak;

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            // 't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", "."),
            't_tarifdasar' => $tarif_pajak,
            // 't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['t_idkorek']);
        $data = array(
//            't_hargapasaran' => number_format($dataRekening['s_tarifdasarkorek'], 0, ",", "."),
            't_tarifpersen' => $dataRekening['s_persentarifkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jmlh_kendaraan']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $t_pajak = $t_jumlah * $data_get['t_tarifpersen'] / 100;
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            't_pajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7']);
        $t_pajak = $t_jmlhpajak;
        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            // 't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", "."),
            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakwaletAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_nilaiperolehan']) * str_ireplace(".", "", $data_get['t_tarifdasarkorek']);
        $t_pajak = $t_jumlah * $data_get['t_tarifpajak'] / 100;

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function cetakhimbauanAction() {
        /** Cetak Himbauan
         * @param int $t_idwp
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 02/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['t_idtransaksi']);

// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetaknppdselfAction() {
        /** Cetak NPPD Self Assesment
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 22/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataNppdObjek($data_get['idwpobjeknppd']);
        $datanppd = $this->Tools()->getService('PendataanTable')->getDataNppdSelf($data_get['idwpobjeknppd']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'datanppd' => $datanppd,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function ambilidkorekAction(){
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_jeniswpreklame = $this->Tools()->getService('ReklameTable')->getHargaReklame($data_get['t_jenisreklame']);
        $data_korek = $this->Tools()->getService('ReklameTable')->getViewKorek($data_jeniswpreklame['s_idkorek']);
        // var_dump($data_get);exit();
        $data_render = array(
            "data_korek" => $data_korek['korek'],
            "t_idkorek" => $data_jeniswpreklame['s_idkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));

    }

    public function cetaknppdofficialAction() {
        /** Cetak NPPD Official Assesment
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 22/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        if ($data_get['jenisobjek'] == 4) {
            $data = $this->Tools()->getService('PenetapanTable')->getDataPenetapanReklame($data_get['idtransaksi']);
            $ar_jenisreklame = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data['t_jenisreklame']);
            // $tarif_nspr = $this->Tools()->getService('ReklameTable')->getTarifNSPR($data['t_klasifikasi_jalan']);
            // $tarif_kawasan = $this->Tools()->getService('ReklameTable')->getDataZona($data['t_wilayah']);
            // $tarif_tinggi = $this->Tools()->getService('ReklameTable')->getTarifTinggi($data['t_tinggi']);
        } elseif ($data_get['jenisobjek'] == 8) {
            $data = $this->Tools()->getService('PenetapanTable')->getDataPenetapanABT($data_get['idtransaksi']);
            // $peruntukan_air = $this->Tools()->getService('AirTable')->getdataPeruntukanairId($data['t_kodekelompok']);
//            if($data['t_hargaberdasarkan'] == 2){
//                $ukuranpipa = $this->Tools()->getService('AirTable')->getdataTarifpipaId($data['t_ukuranpipa']);
//            }
        }
    //    var_dump($data,$data_get['jenisobjek']);
    //    exit();
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $ar_ttd1 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd1']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'ar_ttd0' => $ar_ttd0,
            'ar_ttd1' => $ar_ttd1,
            'jenisobjek' => $data_get['jenisobjek'],
            'ar_jenisreklame' => $ar_jenisreklame,
            'peruntukan_air' => $peruntukan_air,
//            'ukuranpipa' => $ukuranpipa
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function pilihskpdjabatanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_render = array(
            "t_tglpendataan" => date('01-m-Y', strtotime($data_get['parameter'] . '+1 month')),
            "t_masaawal" => date('d-m-Y', strtotime($data_get['parameter'])),
            "t_masaakhir" => date('t-m-Y', strtotime($data_get['parameter'])),
            "t_tarifkenaikan" => 25
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataSuratTeguranAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjekwp']);
        $data_render = array(
            "idobjekwp" => $data_get['idobjekwp'],
            "t_masaawal" => date('d-m-Y', strtotime($data_get['parameter'])),
            "t_masaakhir" => date('t-m-Y', strtotime($data_get['parameter'])),
            "namawp" => $data['t_nama'],
            "alamatwp" => $data['t_alamatlengkapobjek'],
            "jenispajak" => $data['s_namajenis'],
            "idjenispajak" => $data['t_jenisobjek'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetaksuratteguranAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjekwp']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'tglpendataan0' => $data_get->tglpendataan0,
            'tglpendataan1' => $data_get->tglpendataan1,
            'masapajak' => date('d-m-Y', strtotime($data_get->masaawal)) . ' s/d ' . date('d-m-Y', strtotime($data_get->masaakhir)),
            'nomor_surat' => $data_get->nomorsrt,
            'tglcetak' => $data_get->tglcetak,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetakskpdAction() {   
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        if ($data_get['jenisobjek'] == 4) {
            $data = $this->Tools()->getService('PenetapanTable')->getDataPenetapanReklame($data_get['idtransaksi']);
            // $ar_jenisreklame = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data['t_jenisreklame']);
            $ar_jenisreklame = $this->Tools()->getService('ReklameTable')->getDetailJenisReklame($data_get['idtransaksi']);
        } elseif ($data_get['jenisobjek'] == 8) {
            $data = $this->Tools()->getService('PenetapanTable')->getDataPenetapanABT($data_get['idtransaksi']);
        }
                                 
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
         
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'ar_ttd0' => $ar_ttd0,
            'jenisobjek' => $data_get['jenisobjek'],
            'ar_jenisreklame' => $ar_jenisreklame,
            'jmlh_ar_jenisreklame' => count($ar_jenisreklame),
            'peruntukan_air' => $peruntukan_air,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetakskrdAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        // var_dump($data_get['jenisobjek']); die;
        $data = $this->Tools()->getService('PendataanTable')->getPendataanRetribusi($data_get['idtransaksi']);
        $datakorek = $this->Tools()->getService('RekeningTable')->getRekeningParentByKorek(substr($data['korek'], 0, 11));
        ///////////////////////////////////////////////////////////////////////////////////////////////
        if ($data_get['jenisobjek'] == 12) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }
        elseif ($data_get['jenisobjek'] == 13) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }
        elseif ($data_get['jenisobjek'] == 16) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }
        elseif ($data_get['jenisobjek'] == 26) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }
        elseif ($data_get['jenisobjek'] == 27) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }
        elseif ($data_get['jenisobjek'] == 30) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }       
        elseif ($data_get['jenisobjek'] == 34) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }       
        elseif ($data_get['jenisobjek'] == 36) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }       
        elseif ($data_get['jenisobjek'] == 37) {
            $datadetail = $this->Tools()->getService('DetailretribusiTable')->getPendataanPelayananKesehatanByIdTransaksi($data_get['idtransaksi']);
        }
        else
        {
            
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////

        // elseif ($data_get['jenisobjek'] == 17) {
        //     $datadetail = $this->Tools()->getService('DetailpasarTable')->getDataDetailPasar($data_get['idtransaksi']);
        // }
        // elseif ($data_get['jenisobjek'] == 18) {
        //     $datadetail = $this->Tools()->getService('DetailkirTable')->getPendataanPengujianKendaraanBermotorByIdTransaksi($data_get['idtransaksi']);
        // }
        // elseif ($data_get['jenisobjek'] == 40) {
        //     $datadetail = $this->Tools()->getService('DetailtrayekTable')->getPendataanTrayekByIdTransaksi($data_get['idtransaksi']);
        // }
        // elseif ($data_get['jenisobjek'] == 29) {
        //     $datadetail = $this->Tools()->getService('DetailterminalTable')->getPendataanterminalByIdTransaksi($data_get['idtransaksi']);
        // }

        // if ($data['korek'] = '4.1.2.01.06') {
        //     $data['t_tglpenetapan'] = $data['t_tglpendataan'];
        // }
        ////////////////////////////////////////////////////////////////////////////////
        // echo '<pre>';
        // echo ('$data');
        // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($data));
        // print_r($data);
        // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($datakorek));
        // echo '</br>========================================================================';
        // echo ('$datadetail');
        // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($datadetail));
        // echo '</br>========================================================================';
        // echo '</pre>';
         
        // exit();
        ////////////////////////////////////////////////////////////////////////////////
        
        
        
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();

         
        $pdf->setVariables(array(
            'data' => $data,
            'datakorek' => $datakorek,
            'datadetail' => $datadetail,
            'ar_pemda' => $ar_pemda,
            'ar_ttd0' => $ar_ttd0,
            'jenisobjek' => $data_get['jenisobjek']
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetakskAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        $data = $this->Tools()->getService('PendataanTable')->getPendataanRetribusi($data_get['idtransaksi']);
//        var_dump($data['t_periodepajak']); exit();
        if ($data_get['jenisobjek'] == 17) {
            $datadetail = $this->Tools()->getService('DetailpasarTable')->getPendataanPasarByIdTransaksi($data_get['idtransaksi']);
            $datadetailsampah = $this->Tools()->getService('DetailkebersihanTable')->getPendataanSampahByDatapasar($data['t_idwp'], $data);
        }
//        elseif ($data_get['jenisobjek'] == 11) {
//            $datadetail = $this->Tools()->getService('PendataanTable')->getPanggungReklame($data_get['idtransaksi']);
//        }
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'datadetail' => $datadetail,
            'datadetailsampah' => $datadetailsampah,
            'ar_pemda' => $ar_pemda,
            'ar_ttd0' => $ar_ttd0,
            'jenisobjek' => $data_get['jenisobjek']
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetakdaftarreklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        $data = $this->Tools()->getService('PendataanTable')->getDaftarReklame($base);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function formattglAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tlg = (int) substr($data_get->tgl, 0, 2);

        $bln = (int) substr($data_get->tgl, 2, 2);
        if ($bln < 1) {
            $bln = 1;
        } elseif ($bln > 12) {
            $bln = 12;
        }

        $thn = substr($data_get->tgl, 4, 5);
        $tglfix = str_pad($tlg, 2, "0", STR_PAD_LEFT) . "-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        if ($tlg < 1) {
            $tglfix = "01-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        } elseif ($tlg > 28) {
            $tglfix0 = "28-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
            $tglfix = date('t-m-Y', strtotime($tglfix0));
        }
        $data = array(
            'tgl' => $tglfix
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtglAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $tlg = (int) substr($data_get->t_masaawal, 0, 2);

        $bln = (int) substr($data_get->t_masaawal, 2, 2);
        if ($bln < 1) {
            $bln = 1;
        } elseif ($bln > 12) {
            $bln = 12;
        }

        $thn = substr($data_get->t_masaawal, 4, 5);
        $t_masaawal = str_pad($tlg, 2, "0", STR_PAD_LEFT) . "-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        if ($tlg < 1) {
            $t_masaawal = "01-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        } elseif ($tlg > 31) {
            $t_masaawal0 = "31-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
            $t_masaawal = date('t-m-Y', strtotime($t_masaawal0));
        }

        $t_masaakhir = date('t-m-Y', strtotime($t_masaawal));
        $data = array(
            't_masaakhir' => $t_masaakhir,
            't_masaawal' => $t_masaawal
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtahunAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        
        $data_get->t_masaakhir = date('d-m-Y', strtotime($data_get->t_masaawal . 'next year'));
        
        // var_dump($data_get);exit();
        // $t_masaakhir = date('t-m-Y', strtotime($t_masaawal));
        $data = array(
            't_masaakhir' => $data_get->t_masaakhir
            // 't_masaawal' => $t_masaawal
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function cetakregisterpajakAction() {
        $req = $this->getRequest();
        // $data_get = $req->getPost();
        $data_get = $req->getQuery();

        $data = $this->Tools()->getService('PendataanTable')->getDaftarRegisterPajak($data_get['tglinput0'], $data_get['tglinput1'], $data_get['s_idjenis'], $data_get['t_statusbayar'], $data_get['filtertanggal']);
        $ar_jenis = $this->Tools()->getService('ObjekTable')->getJenisObjek($data_get['s_idjenis']);

        $ar_diperiksa = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['diperiksa']);
        $ar_mengetahui = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['mengetahui']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        if ($data_get->formatcetak == 'pdf') {
            $pdf = new \LosPdf\View\Model\PdfModel();
            $pdf->setVariables(array(
                'data' => $data,
                'data_jenis' => $ar_jenis,
                'ar_diperiksa' => $ar_diperiksa,
                'ar_mengetahui' => $ar_mengetahui,
                'ar_pemda' => $ar_pemda,
                'tglcetak' => $data_get['tglcetak'],
                'tglpencetakan' => $data_get['tglinput0'] . ' s.d. ' . $data_get['tglinput1'],
                'tglregister' => 'Tanggal '.$data_get['filtertanggal'],
                'formatcetak' => $data_get->formatcetak
            ));
            $pdf->setOption("paperSize", "legal-L");
            return $pdf;
        } elseif ($data_get->formatcetak == 'excel') {
            $view = new ViewModel(array(
                'data' => $data,
                'data_jenis' => $ar_jenis,
                'ar_diperiksa' => $ar_diperiksa,
                'ar_mengetahui' => $ar_mengetahui,
                'ar_pemda' => $ar_pemda,
                'tglcetak' => $data_get['tglcetak'],
                'tglpencetakan' => $data_get['tglinput0'] . ' s.d. ' . $data_get['tglinput1'],
                'tglregister' => 'Tanggal '.$data_get['filtertanggal'],
                'formatcetak' => $data_get->formatcetak
            ));
            $data = array('nilai' => '3');
            $this->layout()->setVariables($data);
            return $view;
        }
    }

    public function cetakpendterimadimukaAction() {
        $req = $this->getRequest();
        // $data_get = $req->getPost();
        $data_get = $req->getQuery();

        $ar_diperiksa = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['diperiksa']);
        $ar_mengetahui = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['mengetahui']);
        $data = $this->Tools()->getService('PendataanTable')->getDataTransaksiByPerMasaAkhirpajak($data_get['tglcetak'], $data_get['periodepajak'], $data_get['s_idjenispenetapan']);
        // var_dump($data);exit();
        $ar_jenis = $this->Tools()->getService('ObjekTable')->getJenisObjek($data_get['s_idjenispenetapan']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data_jenis' => $ar_jenis,
            'ar_pemda' => $ar_pemda,
            'ar_diperiksa' => $ar_diperiksa,
            'ar_mengetahui' => $ar_mengetahui,
            'tglcetak' => $data_get['tglcetak'],
            'periode' => $data_get['periodepajak']
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function cetakdaftarblmlaporAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $datajenispajak = $this->Tools()->getService('PendataanTable')->getJenisPajak($data_get->s_idjenis);
        $data = $this->Tools()->getService('PendataanTable')->getdaftarbelumlapor($data_get->idobjekbelumlapor);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'datajenispajak' => $datajenispajak,
            'data' => $data,
            'bulan' => $data_get->bulanmasapajak,
            'tahun' => $data_get->tahunmasapajak,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }
    
    
    public function comboJenisReklameAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('ReklameTable')->getDataIdKorek($data_get['t_idkorek']);
        $res = '<option value="">Silahkan Pilih</option>';
        $counter = 1;
        foreach ($data as $row):
            $res .= '<option value="' . $row['s_idtarif'] . '">' . $counter++ . ' || ' . $row['s_namajenis'] . '</option>';
        endforeach;
        
        $dataToRender = [
            'res' => $res,
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($dataToRender));
    }
    
    public function caritarifreklameAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $ar_data = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data_get['t_jenisreklame']);
        $data = array(
            't_tarif' => number_format($ar_data['s_tarif'], 0, ",", "."),
            't_termin' => $ar_data['s_termin'],
            't_tarifpajak' => $ar_data['s_persentarifkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
    
    public function registrasiva__($id){
        $vareg = $this->Tools()->getService('PendataanTable')->getDataVaID($id);

        $jatuhtempo = $vareg['t_tgljatuhtempo'];
        $tglsekarang = date('Y-m-d');

        $ts1 = strtotime($jatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
            
        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);

        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        } else {
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        }
        
        $jmlbunga = ($jmlbunga > 0) ? round($jmlbunga) : 0;

        $blnexp1 = date('Y-m-d', $ts1); //jatuhtempo
        $blnexp2 = date('Y-m-d', $ts2); //sekarang

        $endofmonth = date('Y-m-t');
        //var_dump($endofmonth);exit();

        if($blnexp1 <= $blnexp2){
            $expired = date('Y-m-' . $day1, strtotime("+".$tambahanbulan." month"));
        }else{
            $expired = $blnexp1;
        }

        if($vareg['t_jenispajak']!= 4 || $vareg['t_jenispajak']!= 8 ){
            if($endofmonth < $expired ){
                $expired = $endofmonth;
            }
        }

        if($vareg['t_jenispajak'] == 4 || $vareg['t_jenispajak'] == 8){
            $jenissurat = 2;
        }else{
            $jenissurat = 1;
        }

        
        $nourut = $vareg['t_nourut'];
        if(strlen($nourut) <= 3){
            $nomerrandom = $this->generateRandomString(4).str_pad($nourut, 3, "0", STR_PAD_LEFT);
        }else{
            $nomerrandom = $this->generateRandomString(7);
        }
        
        $createva = $vareg['s_kodebank'].str_pad($vareg['t_jenispajak'],2,'0',STR_PAD_LEFT).str_pad($jenissurat, 2, "0", STR_PAD_LEFT).str_pad($nomerrandom, 7, "0", STR_PAD_LEFT);

        $tglexpva = $vareg['t_tglexpva'];
        $denda = $jmlbunga;
        $nama = $vareg['t_nama'];
        $tagihan = $vareg['t_jmlhpajak'] + $denda;
        $kodebayar = $vareg['t_kodebayar'];
        $masapajak = $vareg['t_masaawal'] .' s/d '. $vareg['t_masaakhir'];
        $pokokpajak = $vareg['t_jmlhpajak'];
        $npwpd = $vareg['t_npwpd'];
        $jenispajak = $vareg['s_namajenis'];

        
        if(empty($vareg['t_tglpembayaran'])){
            if(empty($tglexpva) or empty($kodebayar) or strtotime($tglexpva) < strtotime($blnexp2)){
                $tglexpva = $this->Tools()->getService('PendataanTable')->simpantglexpva($id, $expired);
                $datava = $this->Tools()->getService('PendataanTable')->simpanva($id, $createva);
            }else{
                $tglexpva = $tglexpva;
                $datava = $kodebayar;
            }
        }else{
            $tglexpva = $tglexpva;
            $datava = $kodebayar;
        }

        $tglexp = str_replace("-","" , $tglexpva); 
				
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://jatimva.bankjatim.co.id/Va/RegPen', //Prod
//          CURLOPT_URL => 'https://apps.bankjatim.co.id/Api/Registrasi', //Devel
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POST => true,
          CURLOPT_POSTFIELDS => "{
          \r\n\t\"VirtualAccount\" : \"$datava\",
          \r\n\t\"Nama\" : \"$nama\",
          \r\n\t\"TotalTagihan\" : $tagihan,
          \r\n\t\"TanggalExp\" : \"$tglexp\",
          \r\n\t\"Berita1\" : \"$npwpd\",
          \r\n\t\"Berita2\" : \"$masapajak\",
          \r\n\t\"Berita3\" : \"$pokokpajak\",
          \r\n\t\"Berita4\" : \"$jmlbunga\",
          \r\n\t\"Berita5\" : \"$jenispajak\",
          \r\n\t\"FlagProses\" : 1
          \r\n}",

          CURLOPT_HTTPHEADER => [
            "Content-Type: application/json"
        ],
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_FAILONERROR => true,
        ));
       
        $response = curl_exec($curl);
        
        if (curl_errno($curl)) {
             $error_msg = curl_error($curl);
        }
            curl_close($curl);

        if (isset($error_msg)) {
    // TODO - Handle cURL error accordingly
        }
        // var_dump($response);exit();
        @$decode = json_decode($response, true);
       
        if ($decode['Status'] == 'success') {
            foreach ($decode['data'] as $row) {
                $data = $row;
            }
            $message = "Data Ditemukan";
        } else {
            $data['VirtualAccount'] = '';
            $data['Nama'] = '';
            $message = "Data Tidak Ditemukan";
        }
        
        $data_render = array(
            "VirtualAccount" => $data['VirtualAccount'],
            "Nama" => $data['Nama'],
            "message" => $message
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
    
    public function registrasiva($id, $flaging){
        
        $vareg = $this->Tools()->getService('PendataanTable')->getDataVaID($id);
        
        $jatuhtempo = $vareg['t_tgljatuhtempo'];
        $tglsekarang = date('Y-m-d');

        $ts1 = strtotime($jatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
            
        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);

        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        } else {
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        }
        
        $jmlhbunga = ($jmlbunga > 0) ? round($jmlbunga) : 0;
        
        $blnexp1 = date('Y-m-d', $ts1); //jatuhtempo
        $blnexp2 = date('Y-m-d', $ts2); //sekarang

        $endofmonth = date('Y-m-t');

        if($blnexp1 <= $blnexp2){
            $expired = date('Y-m-' . $day1, strtotime("+".$tambahanbulan." month"));
        }else{
            $expired = $blnexp1;
        }

        if($vareg['t_jenispajak']!= 4 || $vareg['t_jenispajak']!= 8 ){
            if($endofmonth < $expired ){
                $expired = $endofmonth;
            }
        }

        if($vareg['t_jenispajak'] == 4 || $vareg['t_jenispajak'] == 8){
            $jenissurat = 2;
        }else{
            $jenissurat = 1;
        }
 
        $periodepajak = $vareg['t_periodepajak'];
        $nourut = $vareg['t_nourut'];
        if(strlen($nourut) <= 3){
            $nomerrandom = $this->generateRandomString(2).str_pad($nourut, 3, "0", STR_PAD_LEFT);
        }else{
            $nomerrandom = str_pad($nourut, 5, "0", STR_PAD_LEFT);
        }
        
        $createva = $vareg['s_kodebank'].str_pad($vareg['t_jenispajak'],2,'0',STR_PAD_LEFT).str_pad($jenissurat, 2, "0", STR_PAD_LEFT).substr($periodepajak,2).str_pad($nomerrandom, 5, "0", STR_PAD_LEFT);

        $tglexpva = $vareg['t_tglexpva'];
        $denda = $jmlhbunga;
        $nama = substr($vareg['t_nama'],0,50);
        $tagihan = round($vareg['t_jmlhpajak'] + $denda);
        $kodebayar = $vareg['t_kodebayar'];
        $masapajak = $vareg['t_masaawal'] .' s/d '. $vareg['t_masaakhir'];
        $pokokpajak = $vareg['t_jmlhpajak'];
        $npwpd = $vareg['t_namaobjek']; //$vareg['t_npwpd'];
        $jenispajak = $vareg['s_namakorek']; //$vareg['s_namajenis'];
        
        if(empty($vareg['t_tglpembayaran'])){
            if(empty($tglexpva) or empty($kodebayar) or strtotime($tglexpva) < strtotime($blnexp2)){
                $tglexpva = $this->Tools()->getService('PendataanTable')->simpantglexpva($vareg['t_idtransaksi'], $expired);
                $datava = $this->Tools()->getService('PendataanTable')->simpanva($vareg['t_idtransaksi'], $createva);
                $prosesFlag = $flaging;
            }else{
                $tglexpva = $tglexpva;
                $datava = $kodebayar;
                $prosesFlag = $flaging;
            }
        }else{
            $tglexpva = $tglexpva;
            $datava = $kodebayar;
            $prosesFlag = $flaging;
        }
        
        $tglexp = str_replace("-","" , $tglexpva); 
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://jatimva.bankjatim.co.id/Va/RegPen', //prod
		  // CURLOPT_URL => 'https://apps.bankjatim.co.id/Api/Registrasi', //devel
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
				"VirtualAccount": "'.$datava.'",
				"Nama": "'.$nama.'",
				"TotalTagihan": '.$tagihan.',
				"TanggalExp": "'.$tglexp.'",
				"Berita1": "'.substr($npwpd, 0, 30).'",
				"Berita2": "'.substr($masapajak, 0, 30).'",
				"Berita3": "'.substr($pokokpajak, 0, 30).'",
				"Berita4": "'.substr($denda, 0, 30).'",
				"Berita5": "'.substr($jenispajak, 0, 30).'",
				"FlagProses": '.$prosesFlag.'
			}',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
		
		$regMessage = 'Request Register => VirtualAccount: '.$datava.' { '.$response.' }';
		
		$this->Tools()->getService('PendataanTable')->logReg($regMessage);
		
        return $response;
    }
	
	public function vaRegistrationAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->registrasiva($data_get->idtransaksi, 1);

        @$decode = json_decode($data, true);

        $res_error = $decode['Status']['IsError'];
        $res_code = $decode['Status']['ResponseCode'];
        $res_desc = $decode['Status']['ErrorDesc'];

        if($res_code == '05'){
            $ar_data = $this->registrasiva($data_get->idtransaksi, 1);
            @$ar_decode = json_decode($ar_data, true);

            $res_ar_error = $ar_decode['Status']['IsError'];
            $res_ar_code = $ar_decode['Status']['ResponseCode'];
            $res_ar_desc = $ar_decode['Status']['ErrorDesc'];
        }

        if($res_code == '81' || $res_ar_code == '81'){
            $up_data = $this->registrasiva($data_get->idtransaksi, 2);
            @$up_decode = json_decode($up_data, true);

            $res_up_error = $up_decode['Status']['IsError'];
            $res_up_code = $up_decode['Status']['ResponseCode'];
            $res_up_desc = $up_decode['Status']['ErrorDesc'];
        }

        $data_render = array(
            "error" => ($res_code == '05') ? $res_ar_error : (($res_code == '81' || $res_ar_code == '81') ? $res_up_error : $res_error),
            "code" => ($res_code == '05') ? $res_ar_code : (($res_code == '81' || $res_ar_code == '81') ? $res_up_code : $res_code),
            "desc" => ($res_code == '05') ? $res_ar_desc : (($res_code == '81' || $res_ar_code == '81') ? $res_up_desc : $res_desc)
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function generateRandomString($length) {
        $characters = '0123456789'; //abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}