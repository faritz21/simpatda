<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class ParkirTable extends AbstractTableGateway {

    protected $table = 's_tarifparkirtepi';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new ParkirBase());
        $this->initialize();
    }

    public function getdata() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function checkExist(ParkirBase $kc) {
        $rowset = $this->select(array('s_kodekec' => $kc->s_kodekec));
        $row = $rowset->current();
        return $row;
    }

    public function checkId(ParkirBase $kc) {
        $rowset = $this->select(array('s_idreklame' => $kc->s_idreklame));
        $row = $rowset->current();
        return $row;
    }

    public function savedata($kc, $session) {
        $data = array(
            's_keterangan' => $kc->s_keterangan,
            // 's_tarif' => $kc->s_tarif,
            's_satuan' => $kc->s_satuan,
            's_tarif' => str_ireplace(".", "", $kc->s_tarif),
        );
        // var_dump($kc);exit();
        $id = (int) $kc->s_idtarif;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idtarif' => $kc->s_idtarif));
        }
    }

    public function savedatasticker($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_stiker');

        $data = array(
            's_nsrstiker' => $post->s_nsrstiker,
        );
        $update->set($data);
        $id = (int) $post->s_idstiker;
        $where = new Where();
        $where->equalTo('s_idstiker', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function savedataselebaran($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_selebaran');

        $data = array(
            's_nsrselebaran' => $post->s_nsrselebaran,
        );
        $update->set($data);
        $id = (int) $post->s_idselebaran;
        $where = new Where();
        $where->equalTo('s_idselebaran', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function savedataberjalan($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_berjalan');

        $data = array(
            's_nsrberjalan' => $post->s_nsrberjalan,
        );
        $update->set($data);
        $id = (int) $post->s_idberjalan;
        $where = new Where();
        $where->equalTo('s_idberjalan', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function savedatabiayapemasangan($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_biayapemasangan');

        $data = array(
            's_biayapemasangan' => $post->s_biayapemasangan,
        );
        $update->set($data);
        $id = (int) $post->s_jenisreklame;
        $where = new Where();
        $where->equalTo('s_jenisreklame', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function savedatakelompokjalan($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_kelompokjalan');

        $data = array(
            's_hargadasarlokasi' => $post->s_hargadasarlokasi,
            's_skorlokasi' => $post->s_skorlokasi,
        );
        $update->set($data);
        $id = (int) $post->s_idkelompokjalan;
        $where = new Where();
        $where->equalTo('s_idkelompokjalan', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function savedataskorukuran($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_skorukuran');

        $data = array(
            's_skor' => $post->s_skor,
        );
        $update->set($data);
        $id = (int) $post->s_idskorukuran;
        $where = new Where();
        $where->equalTo('s_idskorukuran', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function savedatasudutpandang($post) {
        $sql = new Sql($this->getAdapter());
        $update = $sql->update();
        $update->table('s_reklame_sudutpandang');

        $data = array(
            's_skorsudutpandang' => $post->s_skorsudutpandang,
        );
        $update->set($data);
        $id = (int) $post->s_idsudutpandang;
        $where = new Where();
        $where->equalTo('s_idsudutpandang', $id);
        $update->where($where);
        return $sql->prepareStatementForSqlObject($update)->execute();
    }

    public function checkEmpty() {
        $resultSet = $this->select();
        return $resultSet->count();
    }

    public function getGridCount(ParkirBase $base, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($post->t_keterangan != '')
            $where->literal("s_keterangan ILIKE '%$post->t_keterangan%'");
        if ($post->t_satuan != '')
            $where->literal("s_satuan ILIKE '%$post->t_satuan%'");
        if ($post->t_tarif != '')
            $where->literal("s_tarif::text like '$post->t_tarif%'");
        $select->where($where);
        // echo $select->getSqlString();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(ParkirBase $base, $offset, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($post->t_jenisreklame != '')
            $where->literal("s_keterangan ILIKE '%$post->t_keterangan%'");
        if ($post->t_satuan != '')
            $where->literal("s_satuan ILIKE '%$post->t_satuan%'");
        if ($post->t_tarifreklame != '')
            $where->literal("s_tarif::text like '$post->t_tarif%'");
        $select->where($where);
        $select->order("s_idtarif asc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        // echo $select->getSqlString();
        $res = $state->execute();
        return $res;
    }

    public function getGridStickerCount(ParkirBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_stiker');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridStickerData(ParkirBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_stiker');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            }
        } else {
            $select->order("s_idstiker asc");
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridSelebaranCount(ParkirBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_selebaran');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridSelebaranData(ParkirBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_selebaran');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            }
        } else {
            $select->order("s_idselebaran asc");
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridBerjalanCount(ParkirBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_berjalan');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridBerjalanData(ParkirBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_berjalan');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            }
        } else {
            $select->order("s_idberjalan asc");
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridBiayapemasanganCount(ParkirBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_biayapemasangan');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridBiayapemasanganData(ParkirBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_biayapemasangan');
        $where = new Where();
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("LOWER($base->combocari::text) LIKE LOWER('%$base->kolomcari%')");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            }
        } else {
            $select->order("s_jenisreklame asc");
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idtarif' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getDataIdReklame($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_jenis');
        $where = new Where();
        $where->equalTo('s_idjenis', (int) $id);
        $select->where($where);
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
        return $prep->current();
    }
    
    public function getDataStickerId($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_stiker');
        $where = new Where();
        $where->equalTo('s_idstiker', $id);
        $select->where($where);
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep->current();
    }

    public function getDataSelebaranId($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_selebaran');
        $where = new Where();
        $where->equalTo('s_idselebaran', $id);
        $select->where($where);
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep->current();
    }

    public function getDataBerjalanId($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_berjalan');
        $where = new Where();
        $where->equalTo('s_idberjalan', $id);
        $select->where($where);
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep->current();
    }

    public function getDataBiayapemasanganId($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_biayapemasangan');
        $where = new Where();
        $where->equalTo('s_jenisreklame', $id);
        $select->where($where);
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep->current();
    }

    public function getDataKelompokjalanId($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_kelompokjalan');
        $where = new Where();
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep;
    }

    public function getDataSkorukuran($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_skorukuran');
        $where = new Where();
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep;
    }

    public function getDataSudutpandang($id) {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from('s_reklame_sudutpandang');
        $where = new Where();
        $prep = $sql->prepareStatementForSqlObject($select)->execute();
//        $resultset = new ResultSet();
//        $resultset->initialize($prep);
        return $prep;
    }

    public function getDataKode($kode) {
        $rowset = $this->select(array('s_kodekec' => $kode));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_idreklame' => $id));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

    public function getIdReklame($kd) {
        $resultSet = $this->select(array('s_kodekec' => $kd));
        return $resultSet->current();
    }

    public function getdaftarkecamatan() {
        $sql = "select * from s_kecamatan order by s_kodekec asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataReklameByIdRekening($s_idrekening) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame');
        $select->columns(array(
            's_idreklame', 's_namareklame'
        ));
        $where = new Where();
        $where->equalTo('s_idrekeningreklame', (int) $s_idrekening);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataTarifReklame($t_jenisreklame) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_jenis');
        $where = new Where();
        $where->equalTo('s_idjenis', (int) $t_jenisreklame);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function comboidSudutpandang() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_sudutpandang');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $combo = [];
        foreach ($res as $col => $row):
            $combo[$row['s_idsudutpandang']] = $row['s_idsudutpandang'] .' || '.$row['s_namasudutpandang'];
        endforeach;
        return $combo;
    }

    public function comboidWilayah() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_wilayah');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $combo = [];
        foreach ($res as $col => $row):
            $combo[$row['s_idwilayah']] = $row['s_nama'];
        endforeach;
        return $combo;
    }

    public function comboidZona() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_index_zona');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $combo = [];
        foreach ($res as $col => $row):
            $combo[$row['s_idindex']] = $row['s_idindex'] .' || '.$row['s_keterangan'];
        endforeach;
        return $combo;
    }

    public function comboidJenisReklame() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_harga');
        $select->order('s_idreklame asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $combo = [];
        $counter = 1;
        foreach ($res as $col => $row):
            $combo[$row['s_idreklame']] = $counter++ .' || '.$row['s_jeniswpreklame'];
        endforeach;
        return $combo;
    }

    public function comboidKlasifikasijalan() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_klarifikasi_jalan');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $combo = [];
        foreach ($res as $col => $row):
            $combo[$row['s_idklj']] = $row['s_idklj'] .' || '.$row['s_nama'];
        endforeach;
        return $combo;
    }
    
    public function getTarifTinggi($tinggi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_tarif_tinggi');
        $where = new Where();
        $where->literal('s_tinggi_min <= ' . $tinggi . ' and s_tinggi_max > ' . $tinggi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    

    public function getIndexZona($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_index_zona');
        $where = new Where();
        $where->equalTo('s_idindex', (int) $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getIndexMuka($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_sudutpandang');
        $where = new Where();
        $where->equalTo('s_idsudutpandang', (int) $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getIndexTinggi($tinggi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_index_tinggi');
        $where = new Where();
        if($tinggi == 3){
            $where->literal('s_tinggi_min <= ' . $tinggi . ' and s_tinggi_max <= ' . $tinggi);
        }else{
            $where->literal('s_tinggi_min <= ' . $tinggi . ' and s_tinggi_max >= ' . $tinggi);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }


    public function getTarifNJOPR($s_idkorek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_njopr');
        $where = new Where();
        $where->equalTo('s_idkorek', (int) $s_idkorek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }


    public function getTarifReklame($idreklame) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_tarif');
        $where = new Where();
        $where->equalTo('s_idtarif', (int)$idreklame);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getDataZonaWIlayah($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_zonawilayah');
        $where = new Where();
        $where->equalTo('s_zona', (int) $id);
        $select->where($where);
        $select->order('s_nourut asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getTarifNSPR($idnspr) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_tarif_klarifikasi');
        $where = new Where();
        $where->equalTo('s_idtarif', (int)$idnspr);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getTarifKawasan($idkawasan) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_tarif_kawasan');
        $where = new Where();
        $where->equalTo('s_idtarif',(int)$idkawasan);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getDataZona($idkawasan) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_index_zona');
        $where = new Where();
        $where->equalTo('s_idindex',(int) $idkawasan);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getTarifSticker($luas) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_stiker');
        $where = new Where();
        $where->literal('s_luasstiker_min <= ' . $luas . ' and s_luasstiker_max >= ' . $luas);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getTarifSelebaran($luas) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_selebaran');
        $where = new Where();
        $where->literal('s_luasselebaran_min <= ' . $luas . ' and s_luasselebaran_max >= ' . $luas);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getTarifBerjalan($jeniskendaraan, $tipewaktu) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_berjalan');
        $where = new Where();
        $where->equalTo('s_jeniskendaraan', $jeniskendaraan);
        $where->equalTo('s_masareklame', $tipewaktu);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKoefisienJalan($kelompokjalan) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_kelompokjalan');
        $where = new Where();
        $where->equalTo('s_idkelompokjalan', $kelompokjalan);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKoefisienJenis($jenisreklame, $jenisreklameusaha, $luas) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_koefisienjenis');
        $where = new Where();
        $where->equalTo('s_jenisreklame', $jenisreklame);
        $where->equalTo('s_tipereklame', $jenisreklameusaha);
        $where->literal("s_luasreklame_min < " . $luas . " and s_luasreklame_max >= $luas");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKoefisienSudutpandang($sudutpandang) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_sudutpandang');
        $where = new Where();
        $where->equalTo('s_idsudutpandang', $sudutpandang);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getBiayaPemasangan($jenisreklame) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_biayapemasangan');
        $where = new Where();
        $where->equalTo('s_jenisreklame', $jenisreklame);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKoefisienLamapemasangan($masareklame) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_koefisienpemasangan');
        $where = new Where();
        $where->literal("s_lamapemasangan_min <=" . $masareklame . " and s_lamapemasangan_max >=" . $masareklame);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKoefisienUkuran($luas) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_skorukuran');
        $where = new Where();
        $where->literal("s_ukuran_min <=" . $luas . " and s_ukuran_max >=" . $luas);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    // tambahan ade
    public function getHargaReklame($idreklame) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_harga');
        $where = new Where();
        $where->equalTo('s_idreklame', (int)$idreklame);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataKorek($s_idkorek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_reklame_jenis');
        $where = new Where();
        $where->equalTo('s_idkorek', (int) $s_idkorek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getViewKorek($idkorek){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_rekening');
        $where = new Where();
        $where->equalTo('s_idkorek', (int) $idkorek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    // end
    
}
