<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailreklameTable extends AbstractTableGateway {

    protected $table = 't_detailreklame';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;    
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailreklameBase());
        $this->initialize();
    }

    public function hapusdetailreklame($t_idtransaksi)
    {
        $this->delete(array('t_idtransaksi' => $t_idtransaksi));
    }

    public function simpanpendataanreklame($datapost, $dataparent) {
        
        $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
        
        for ($i = 0; $i < count($datapost['t_jenisreklame']); $i++) {
            if (!empty($datapost['t_jenisreklame'][$i])) {

                $data = array(
                    't_idtransaksi' => $dataparent['t_idtransaksi'],
                    't_idkorek' => $datapost['t_idkorek'],
                    't_jenisreklame' => $datapost['t_jenisreklame'][$i],
                    't_naskah' => $datapost['t_naskah'][$i],
                    't_lokasi' => $datapost['t_lokasi'][$i],
                    't_tarifreklame' => $datapost['t_tarifpajak'][$i],
                    't_panjang' => $datapost['t_panjang'][$i],
                    't_lebar' => $datapost['t_lebar'][$i],
                    't_jumlah' => $datapost['t_jmlhreklame'][$i],
                    't_jangkawaktu' => $datapost['t_masareklame'][$i],
                    't_tipewaktu' => $datapost['t_tipewaktu'][$i],
                    't_sudutpandang' => $datapost['t_sudutpandang'][$i],
                    't_nsr' => str_ireplace(".", "", $datapost['t_nsr'][$i]),
                    't_jmlhpajaksub' => str_ireplace(".", "", $datapost['t_jmlhpajaksub'][$i]),
                );

                $this->insert($data);
            }
        }
        return $data;
    }

    public function getDetailReklameByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

}
