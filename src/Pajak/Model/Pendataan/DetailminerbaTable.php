<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailminerbaTable extends AbstractTableGateway {       

    protected $table = 't_detailminerba';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function simpanpendataanminerba($datapost, $dataparent) {
 
        $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
         
        for ($i = 0; $i < count($datapost['t_idkorek']); $i++) {
            if (!empty($datapost['t_idkorek'])) {
                $data = array(
                    't_idtransaksi' => $dataparent['t_idtransaksi'],
                    't_idkorek' => $datapost['t_idkorek'],
                    't_volume' => str_ireplace(",", ".", $datapost['t_volume'][$i]),
                    't_hargapasaran' => str_ireplace(".", "", $datapost['t_hargapasaran'][$i]),
                    't_jumlah' => str_ireplace(".", "", $datapost['t_jumlah'][$i]),
                    't_tarifpersen' => str_ireplace(".", "", $datapost['t_tarifpersen'][$i]),
                    't_pajak' => str_ireplace(".", "", $datapost['t_pajak'][$i]),
                );
                $this->insert($data);
            }
        }
    }
    
    public function simpanpendataanrabminerba($datapost, $dataparent) {
 
        $tabel_rab = new \Zend\Db\TableGateway\TableGateway('t_detailrabminerba', $this->adapter);
        $tabel_rab->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
         
        for ($i = 0; $i < count($datapost['t_idrab']); $i++) {
            if (!empty($datapost['t_idrab'][$i])) {
                $data = array(
                    't_idtransaksi' => $dataparent['t_idtransaksi'],
                    't_idrab' => $datapost['t_idrab'][$i],
                    't_volumerab' => $datapost['t_volumerab'][$i],
                    't_batupecah' => $this->pecahanDecimal($datapost['t_batupecah'][$i]),
                    't_batublok' => $this->pecahanDecimal($datapost['t_batublok'][$i]),
                    't_tanahurug' => $this->pecahanDecimal($datapost['t_tanahurug'][$i]),
                    't_sirbang' => $this->pecahanDecimal($datapost['t_sirbang'][$i]),
                    't_sirug' => $this->pecahanDecimal($datapost['t_sirug'][$i]),
                    't_grosok' => $this->pecahanDecimal($datapost['t_grosok'][$i]),
                    't_sirtu' => $this->pecahanDecimal($datapost['t_sirtu'][$i]),
                    't_kapurpasang' => $this->pecahanDecimal($datapost['t_kapurpasang'][$i]),
                    't_tanahliat' => $this->pecahanDecimal($datapost['t_tanahliat'][$i]),
                );
                $tabel_rab->insert($data);
            }
        }
    }
    
    public function simpanpendataannonrabminerba($datapost, $dataparent) {
 
        $tabel_nonrab = new \Zend\Db\TableGateway\TableGateway('t_detailnonrabminerba', $this->adapter);
        $tabel_nonrab->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
         
        for ($i = 0; $i < count($datapost['t_idnonrab']); $i++) {
            if (!empty($datapost['t_idnonrab'][$i])) {
                $data = array(
                    't_idtransaksi' => $dataparent['t_idtransaksi'],
                    't_idnonrab' => $datapost['t_idnonrab'][$i],
                    't_volumenonrab' => str_ireplace(",", ".", $datapost['t_volumenonrab'][$i]),
                    't_indexnonrab' => str_ireplace(",", ".", $datapost['t_indexnonrab'][$i]),
                    't_jumlahnonrab' => str_ireplace(",", ".", $datapost['t_jumlahnonrab'][$i]),
                    't_tarifindexnonrab' => str_ireplace(",", ".", str_ireplace(".", "", $datapost['t_tarifindexnonrab'][$i])),
                    't_pajaknonrab' => str_ireplace(",", ".", str_ireplace(".", "", $datapost['t_pajaknonrab'][$i])),
                    't_pajaknonrab_bulat' => round(str_ireplace(",", ".", str_ireplace(".", "", $datapost['t_pajaknonrab'][$i])))
                );
                $tabel_nonrab->insert($data);
            }
        }
    }

    public function getPendataanMinerbaByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getDetailRabMinerbaByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailrabminerba"
        ));
        $select->join(array(
            "b" => "s_indexrab_minerba"
                ), "a.t_idrab = b.s_idrab", array(
            "s_jenispekerjaan"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $select->order("t_idrab");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getDetailNonRabMinerbaByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailnonrabminerba"
        ));
        $select->join(array(
            "b" => "s_tarifnonrab_minerba"
                ), "a.t_idnonrab = b.s_idnonrab", array(
            "s_jenisminerba"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $select->order("t_idnonrab");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getIndexRab() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_indexrab_minerba");
        $where = new Where();
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getIndexNonRab() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_tarifnonrab_minerba");
        $where = new Where();
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getTarifIndexNonRabById($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_tarifnonrab_minerba");
        $where = new Where();
        $where->equalTo('s_idnonrab', (int) $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getIndexRabById($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_indexrab_minerba");
        $where = new Where();
        $where->equalTo('s_idrab', (int) $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getTarifRab() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_tarifrab_minerba");
        $where = new Where();
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDetailMinerbaByIdTransaksi($t_idtransaksi) {
     $sql = "SELECT t.*, v.korek, v.s_namakorek FROM t_detailminerba t
     LEFT JOIN view_rekening v on v.s_idkorek = t.t_idkorek
     WHERE t_idtransaksi = ".$t_idtransaksi."";
      
     $statement = $this->adapter->query($sql);
     return \Zend\Stdlib\ArrayUtils::iteratorToArray($statement->execute());
    }
    
	function pecahanDecimal($float){
        $res = str_ireplace(",", ".", str_ireplace(".", "", $float));
        return $res;
    }

}
