<?php

namespace Pajak\Model\Pendataan;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DetailairBase implements InputFilterAwareInterface {

    public $t_idair, $t_idtransaksi, $t_volume, $t_hargaberdasarkan, $t_kodekelompok, $t_ukuranpipa, $t_jmlhsumur;
    public $t_volumeair0, $t_hargadasar0, $t_tarif0, $t_jumlah0;
    public $t_volumeair1, $t_hargadasar1, $t_tarif1, $t_jumlah1;
    public $t_volumeair2, $t_hargadasar2, $t_tarif2, $t_jumlah2;
    public $t_volumeair3, $t_hargadasar3, $t_tarif3, $t_jumlah3;
    public $t_volumeair4, $t_hargadasar4, $t_tarif4, $t_jumlah4;
    public $t_volumeair5, $t_hargadasar5, $t_tarif5, $t_jumlah5;
    public $t_volumeair6, $t_hargadasar6, $t_tarif6, $t_jumlah6;
    public $t_fna, $t_tarifdasarkorek, $t_perhitungan, $t_totalnpa;
    
    public $page, $direction, $combocari, $kolomcari, $combosorting, $sortasc, $sortdesc, $combooperator;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->t_idair = (isset($data['t_idair'])) ? $data['t_idair'] : null;
        $this->t_idtransaksi = (isset($data['t_idtransaksi'])) ? $data['t_idtransaksi'] : null;
        $this->t_volume = (isset($data['t_volume'])) ? $data['t_volume'] : null;
        $this->t_hargaberdasarkan = (isset($data['t_hargaberdasarkan'])) ? $data['t_hargaberdasarkan'] : null;
        $this->t_kodekelompok = (isset($data['t_kodekelompok'])) ? $data['t_kodekelompok'] : null;
        $this->t_ukuranpipa = (isset($data['t_ukuranpipa'])) ? $data['t_ukuranpipa'] : null;
        $this->t_jmlhsumur = (isset($data['t_jmlhsumur'])) ? $data['t_jmlhsumur'] : null;
        $this->t_volumeair0 = (isset($data['t_volumeair0'])) ? $data['t_volumeair0'] : null;
        $this->t_hargadasar0 = (isset($data['t_hargadasar0'])) ? $data['t_hargadasar0'] : null;
        $this->t_tarif0 = (isset($data['t_tarif0'])) ? $data['t_tarif0'] : null;
        $this->t_jumlah0 = (isset($data['t_jumlah0'])) ? $data['t_jumlah0'] : null;
        $this->t_volumeair1 = (isset($data['t_volumeair1'])) ? $data['t_volumeair1'] : null;
        $this->t_hargadasar1 = (isset($data['t_hargadasar1'])) ? $data['t_hargadasar1'] : null;
        $this->t_tarif1 = (isset($data['t_tarif1'])) ? $data['t_tarif1'] : null;
        $this->t_jumlah1 = (isset($data['t_jumlah1'])) ? $data['t_jumlah1'] : null;
        $this->t_volumeair2 = (isset($data['t_volumeair2'])) ? $data['t_volumeair2'] : null;
        $this->t_hargadasar2 = (isset($data['t_hargadasar2'])) ? $data['t_hargadasar2'] : null;
        $this->t_tarif2 = (isset($data['t_tarif2'])) ? $data['t_tarif2'] : null;
        $this->t_jumlah2 = (isset($data['t_jumlah2'])) ? $data['t_jumlah2'] : null;
        $this->t_volumeair3 = (isset($data['t_volumeair3'])) ? $data['t_volumeair3'] : null;
        $this->t_hargadasar3 = (isset($data['t_hargadasar3'])) ? $data['t_hargadasar3'] : null;
        $this->t_tarif3 = (isset($data['t_tarif3'])) ? $data['t_tarif3'] : null;
        $this->t_jumlah3 = (isset($data['t_jumlah3'])) ? $data['t_jumlah3'] : null;
        $this->t_volumeair4 = (isset($data['t_volumeair4'])) ? $data['t_volumeair4'] : null;
        $this->t_hargadasar4 = (isset($data['t_hargadasar4'])) ? $data['t_hargadasar4'] : null;
        $this->t_tarif4 = (isset($data['t_tarif4'])) ? $data['t_tarif4'] : null;
        $this->t_jumlah4 = (isset($data['t_jumlah4'])) ? $data['t_jumlah4'] : null;
        $this->t_volumeair5 = (isset($data['t_volumeair5'])) ? $data['t_volumeair5'] : null;
        $this->t_hargadasar5 = (isset($data['t_hargadasar5'])) ? $data['t_hargadasar5'] : null;
        $this->t_tarif5 = (isset($data['t_tarif5'])) ? $data['t_tarif5'] : null;
        $this->t_jumlah5 = (isset($data['t_jumlah5'])) ? $data['t_jumlah5'] : null;
        $this->t_volumeair6 = (isset($data['t_volumeair6'])) ? $data['t_volumeair6'] : null;
        $this->t_hargadasar6 = (isset($data['t_hargadasar6'])) ? $data['t_hargadasar6'] : null;
        $this->t_tarif6 = (isset($data['t_tarif6'])) ? $data['t_tarif6'] : null;
        $this->t_jumlah6 = (isset($data['t_jumlah6'])) ? $data['t_jumlah6'] : null;
        $this->t_fna = (isset($data['t_fna'])) ? $data['t_fna'] : null;
        $this->t_tarifdasarkorek = (isset($data['t_tarifdasarkorek'])) ? $data['t_tarifdasarkorek'] : null;
        $this->t_perhitungan = (isset($data['t_perhitungan'])) ? $data['t_perhitungan'] : null;
        $this->t_totalnpa = (isset($data['t_totalnpa'])) ? $data['t_totalnpa'] : null;

        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->combocari = (isset($data['combocari'])) ? $data['combocari'] : null;
        $this->kolomcari = (isset($data['kolomcari'])) ? $data['kolomcari'] : null;
        $this->combosorting = (isset($data['combosorting'])) ? $data['combosorting'] : null;
        $this->sortasc = (isset($data['sortasc'])) ? $data['sortasc'] : null;
        $this->sortdesc = (isset($data['sortdesc'])) ? $data['sortdesc'] : null;
        $this->combooperator = (isset($data['combooperator'])) ? $data['combooperator'] : null;

        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
