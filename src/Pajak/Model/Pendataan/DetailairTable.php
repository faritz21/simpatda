<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailairTable extends AbstractTableGateway {

    protected $table = 't_detailair';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailairBase());
        $this->initialize();
    }

    public function simpanpendataanair(DetailairBase $base, $dataparent) {
        
        if(!empty($base->t_volume)){
            $t_volume = $base->t_volume;
        }else{
            $t_volume = 0;
        }       
        
        
        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_volume' => $t_volume, //str_ireplace(".", "", $base->t_volume),
            // 't_kodekelompok' => $base->t_kodekelompok,
            // 't_volumeair0' => str_ireplace(".", "", $base->t_volumeair0),
            // 't_hargadasar0' => str_ireplace(".", "", $base->t_hargadasar0),
            // 't_tarif0' => $base->t_tarif0,
            // 't_jumlah0' => str_ireplace(".", "", $base->t_jumlah0),
            // 't_volumeair1' => str_ireplace(".", "", $base->t_volumeair1),
            // 't_hargadasar1' => str_ireplace(".", "", $base->t_hargadasar1),
            // 't_tarif1' => $base->t_tarif1,
            // 't_jumlah1' => str_ireplace(".", "", $base->t_jumlah1),
            // 't_volumeair2' => str_ireplace(".", "", $base->t_volumeair2),
            // 't_hargadasar2' => str_ireplace(".", "", $base->t_hargadasar2),
            // 't_tarif2' => $base->t_tarif2,
            // 't_jumlah2' => str_ireplace(".", "", $base->t_jumlah2),
            // 't_volumeair3' => str_ireplace(".", "", $base->t_volumeair3),
            // 't_hargadasar3' => str_ireplace(".", "", $base->t_hargadasar3),
            // 't_tarif3' => $base->t_tarif3,
            // 't_jumlah3' => str_ireplace(".", "", $base->t_jumlah3),
            // 't_volumeair4' => str_ireplace(".", "", $base->t_volumeair4),
            // 't_hargadasar4' => str_ireplace(".", "", $base->t_hargadasar4),
            // 't_tarif4' => $base->t_tarif4,
            // 't_jumlah4' => str_ireplace(".", "", $base->t_jumlah4),
            // 't_volumeair5' => str_ireplace(".", "", $base->t_volumeair5),
            // 't_hargadasar5' => str_ireplace(".", "", $base->t_hargadasar5),
            // 't_tarif5' => $base->t_tarif5,
            // 't_jumlah5' => str_ireplace(".", "", $base->t_jumlah5),
            // 't_volumeair6' => str_ireplace(".", "", $base->t_volumeair6),
            // 't_hargadasar6' => str_ireplace(".", "", $base->t_hargadasar6),
            // 't_tarif6' => $base->t_tarif6,
            // 't_jumlah6' => str_ireplace(".", "", $base->t_jumlah6),
//            't_fna' => $base->t_fna,
            't_tarifdasarkorek' => str_ireplace(".", "", $base->t_tarifdasarkorek),
            't_perhitungan' => $base->t_perhitungan,
            't_totalnpa' => str_ireplace(".", "", $base->t_totalnpa),
        );
        $t_idair = $base->t_idair;
        if (empty($t_idair)) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idair' => $t_idair));
        }
        return $data;
    }

    public function getDetailAirByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanairByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

}
