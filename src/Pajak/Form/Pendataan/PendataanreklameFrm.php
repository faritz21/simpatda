<?php

namespace Pajak\Form\Pendataan;

use Zend\Form\Form;

class PendataanreklameFrm extends Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));
        $this->add(array(
            'name' => 't_iddetailreklame',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_iddetailreklame',
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));

        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_nspr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nspr',
                'class' => 'form-control',
                'readonly' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_njopr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_njopr',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));

        $this->add(array(
            'name' => 't_nsr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nsr',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y')
            )
        ));

        $this->add(array(
            'name' => 't_tglpendataan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpendataan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaawal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaawal',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaakhir',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaakhir',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_korek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_korek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_namakorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakorek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));
		
        $this->add(array(
            'name' => 't_naskah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_naskah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_lokasi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lokasi',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_panjang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_panjang',
                'class' => 'form-control',
                'required' => true,
                'value' => 1,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_lebar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lebar',
                'class' => 'form-control',
                'required' => true,
                'value' => 1,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_luas',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luas',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));
        
        $this->add(array(
            'name' => 't_tinggi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tinggi',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_arah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_arah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_sudutpandang',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_sudutpandang',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah',
                'class' => 'form-control',
                'required' => true,
                'value' => 1,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_jangkawaktu',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jangkawaktu',
                'class' => 'form-control',
                'value' => 1,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_tarifreklame',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifreklame',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'readonly' => true
            )
        ));

            $this->add(array(
            'name' => 't_tipewaktu',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tipewaktu',
                'class' => 'form-control',
                'readonly' => true
            ),
        ));

        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:#000099; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));

        $this->add(array(
            'name' => 't_tglpenetapan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpenetapan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 'Pendataansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Pendataansubmit',
                'class' => "btn btn-warning btn-block",
                'onclick'=>'validateAndRun(this.form);this.disabled=true;'     
            )
        ));
    }
}
