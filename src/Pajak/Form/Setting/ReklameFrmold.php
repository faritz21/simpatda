<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class ReklameFrm extends Form {
    
    public function __construct($cmb_rekening = null) {
        parent::__construct();
        
        $this->setAttribute("method", "post");
        
        $this->add(array(
            'name' => 's_idreklame',
            'type' => 'hidden',
            'attributes' => array(             
                'id'=>'s_idreklame'
            )
        ));
        
        $this->add(array(
            'name' => 's_jeniswpreklame',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_jeniswpreklame',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_idkorek',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_idkorek',                
                'class'=>'form-control'
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options'=> $cmb_rekening,
            )
        ));
        
        $this->add(array(
            'name' => 's_ket',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_ket',
                'value'=>'Tahunan',
                'class'=>'form-control disabled',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_tarifreklame',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_tarifreklame',
                'class'=>'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'this.value = formatCurrency(this.value);',
                'onblur' => 'this.value = formatCurrency(this.value);',
                'onkeyup' => 'this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));
        
        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary btn-sm',
            ),
        ));        
    }
    
}