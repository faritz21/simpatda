<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class PasarFrm extends Form {
    
    public function __construct($cmb_rekening = null) {
        parent::__construct();
        
        $this->setAttribute("method", "post");
        
        $this->add(array(
            'name' => 's_idkorek',
            'type' => 'hidden',
            'attributes' => array(             
                'id'=>'s_idkorek'
            )
        ));
        
        $this->add(array(
            'name' => 's_namakorek',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_namakorek',
                'class'=>'form-control disabled',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_persentarifkorek',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_persentarifkorek',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_tarifdasarkorek',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_tarifdasarkorek',
                'class'=>'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'this.value = formatCurrency(this.value);',
                'onblur' => 'this.value = formatCurrency(this.value);',
                'onkeyup' => 'this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));
        
        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary btn-sm',
            ),
        ));        
    }
    
}