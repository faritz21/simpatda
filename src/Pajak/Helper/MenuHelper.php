<?php

namespace Pajak\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use DOMPDFModule\View\Model\PdfModel;

class MenuHelper extends AbstractHelper implements ServiceLocatorAwareInterface {

    protected $tbl;

    public function __invoke() {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function getDataPemda() {
        $ar_pemda = $this->gettbl("PemdaTable")->getdata();
        return $ar_pemda;
    }

    public function gettbl($tbl_service) {
        $this->tbl = $this->getServiceLocator()->getServiceLocator()->get($tbl_service);
        return $this->tbl;
    }

    public function getBelumDitetapkan(){
        $data = $this->gettbl("PenetapanTable")->getBelumDitetapkan();
        return $data;
    }
    

    public function kekata($x) {
        $x = abs($x);
        $angka = array("", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 4) {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }
    
    function namabulan($id){
        $abulan = ['1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        return $abulan[$id];
    }
    
    function namabulandate($id){
        $abulan = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        return $abulan[$id];
    }

    public function hitungDenda($tglcetak,$tgljatuhtempo,$jmlhpajak) {
        $jatuhtempo = date('Y-m-d', strtotime($tgljatuhtempo));
        $tglsekarang = date('Y-m-d', strtotime($tglcetak));

        $ts1 = strtotime($jatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } else {
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        }
        
        return ($jmldenda > 0) ? round($jmldenda) : 0;
    }
    
    public function registrasiva($vareg){
        
        $jatuhtempo = $vareg['t_tgljatuhtempo'];
        $tglsekarang = date('Y-m-d');

        $ts1 = strtotime($jatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
            
        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);

        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        } else {
            $jmlbunga = $jmlbulan * 2 / 100 * $vareg['t_jmlhpajak'];
        }
        
        $jmlbunga = ($jmlbunga > 0) ? round($jmlbunga) : 0;
        
        $blnexp1 = date('Y-m-d', $ts1); //jatuhtempo
        $blnexp2 = date('Y-m-d', $ts2); //sekarang

        $endofmonth = date('Y-m-t');

        if($blnexp1 <= $blnexp2){
            $expired = date('Y-m-' . $day1, strtotime("+".$tambahanbulan." month"));
        }else{
            $expired = $blnexp1;
        }

        if($vareg['t_jenispajak']!= 4 || $vareg['t_jenispajak']!= 8 ){
            if($endofmonth < $expired ){
                $expired = $endofmonth;
            }
        }

        if($vareg['t_jenispajak'] == 4 || $vareg['t_jenispajak'] == 8){
            $jenissurat = 2;
        }else{
            $jenissurat = 1;
        }
 
        $periodepajak = $vareg['t_periodepajak'];
        $nourut = $vareg['t_nourut'];
        if(strlen($nourut) <= 3){
            $nomerrandom = $this->generateRandomString(2).str_pad($nourut, 3, "0", STR_PAD_LEFT);
        }else{
            $nomerrandom = str_pad($nourut, 5, "0", STR_PAD_LEFT);
        }
        
        $createva = $vareg['s_kodebank'].str_pad($vareg['t_jenispajak'],2,'0',STR_PAD_LEFT).str_pad($jenissurat, 2, "0", STR_PAD_LEFT).substr($periodepajak,2).str_pad($nomerrandom, 5, "0", STR_PAD_LEFT);

        $tglexpva = $vareg['t_tglexpva'];
        $denda = $jmlbunga;
        $nama = $vareg['t_nama'];
        $tagihan = $vareg['t_jmlhpajak'] + $denda;
        $kodebayar = $vareg['t_kodebayar'];
        $masapajak = $vareg['t_masaawal'] .' s/d '. $vareg['t_masaakhir'];
        $pokokpajak = $vareg['t_jmlhpajak'];
        $npwpd = $vareg['t_npwpd'];
        $jenispajak = $vareg['s_namajenis'];
        
        if(empty($vareg['t_tglpembayaran'])){
            if(empty($tglexpva) or empty($kodebayar) or strtotime($tglexpva) < strtotime($blnexp2)){
                $tglexpva = $tglexpva;//$this->gettbl("PendataanTable")->simpantglexpva($vareg['t_idtransaksi'], $expired);
                $datava = $kodebayar;//$this->gettbl("PendataanTable")->simpanva($vareg['t_idtransaksi'], $createva);
            }else{
                $tglexpva = $tglexpva;
                $datava = $kodebayar;
            }
        }else{
            $tglexpva = $tglexpva;
            $datava = $kodebayar;
        }
        
        $tglexp = str_replace("-","" , $tglexpva); 
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://apps.bankjatim.co.id/Api/Registrasi',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
        "VirtualAccount": "'.$kodebayar.'",
        "Nama": "'.$nama.'",
        "TotalTagihan": '.$tagihan.',
        "TanggalExp": "'.$tglexp.'",
        "Berita1": "'.$npwpd.'",
        "Berita2": "'.$masapajak.'",
        "Berita3": "'.$pokokpajak.'",
        "Berita4": "'.$denda.'",
        "Berita5": "'.$jenispajak.'",
        "FlagProses": 1
        }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res = $response;
        
        return $res;
    }
    
    public function generateRandomString($length) {
        $characters = '0123456789'; //abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}